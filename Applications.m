/* Algorithms which can be used in application of signature Gröbner bases
 */

LT := LeadingTerm;
LM := LeadingMonomial;
LC := LeadingCoefficient;

intrinsic _GB_with_algo(F::SeqEnum[RngMPolElt], algo::MonStgElt
			: interreduce := true)
	  -> SeqEnum, SeqEnum, SeqEnum
{ Wrapper around the different signature algorithms 

(internal function)}
    case algo:
    when "KRK":
	return GB_KandriRodyKapur(F : interreduce := interreduce);
    when "Lichtblau":
	return GB_Lichtblau(F : interreduce := interreduce);
    else
	error "Algorithm non recognized";
    end case;
end intrinsic;


intrinsic CollectReductions(s::Sig, f::RngMPolElt, G::SeqEnum[Tup]
			   : mod_red := true, tail := false)
	  -> RngMPolElt, SeqEnum[RngMPolElt]
{ Regular reduce f with signature s modulo G. Returns the reduced
polynomial, and the list of reductions performed.

The behaviour is similar to Magma's NormalForm.}
    P := Parent(f);
    res := P!0;
    reds := [P!0 : g in G];
    ff := f;
    first := true;
    while (first or tail) and ff ne 0 do
	ltf := LeadingTerm(ff);
	changed := false;
	for i in [1..#G] do
	    ss,g := Explode(G[i]);
	    q,r := Quotrem_abs(ltf, LeadingTerm(g));
	    if q ne 0 and (mod_red or r eq 0)
	       and SigCmpProd(ss,q,s) lt 0 then
		ff -:= q*g;
		reds[i] +:= q;
		changed := true;
		break i;
	    end if;
	end for;
	if not changed then
	    first := false;
	    if tail then
		res +:= ltf;
		ff -:= ltf;
	    else
		res := ff;
	    end if;
	end if;
    end while;
    return res, reds;
end intrinsic;

intrinsic CollectCoordinates(s::Sig,f::RngMPolElt,G::SeqEnum,GC::SeqEnum)
	  -> SeqEnum
{ Compute partial coordinates wrt the input polynomials

Input:
- f a polynomial
- s a signature
- G a Sig-GB of <f1,...,fm> up to signature s
- GC the coordinates of the elements of G in terms of the fi

Output:
- gg, c such that gg is not s-reducible modulo G, and gg = f + sum(ci fi)

(internal function)
 }
    P := Parent(f);
    npols := #GC[1];
    res := [P!0 : i in [1..npols]];
    gg, reds := CollectReductions(s,f,G);
    for ired in [1..#reds] do
	for ipol in [1..npols] do
	    if reds[ired] ne 0 then
		res[ipol] -:= reds[ired] * GC[ired][ipol];
	    end if;
	end for;
    end for;
    return gg, res;
end intrinsic;

intrinsic FullRepr_GB(F::SeqEnum,Gs::SeqEnum) -> SeqEnum, SeqEnum
{ Compute a GB with coordinates

Input:
- F sequence of polynomials
- Gs Sig-GB of the ideal generated by F

Output:
- G Sig-GB of the ideal generated by F
- GC such that for all i, GC[i] are the coordinates of G[i] in terms
  of F
}
    npols := #F;
    P := Parent(F[1]);
    SigType := Type(Gs[1][1]);
    mods := [[i eq j select P!1 else P!0 : j in [1..npols]]
	    : i in [1..npols]];
    Gpart := [<SigNew(SigType,i,P!1), F[i]>
	    : i in [1..npols]];
    for j->g in Gs do
	cands := [];
	s := g[1];
	for jj in [1..#Gpart] do
	    test, dt := IsDivisibleBy(s,Gpart[jj][1]);
	    if test then
		Append(~cands,<dt*LT(Gpart[jj][2]),dt,jj>);
	    end if;
	end for;
	Sort(~cands);
	_,dt,jj := Explode(cands[1]);
	f := dt*Gpart[jj][2];
	gg, coord := CollectCoordinates(s,f,Gpart,mods);
	for i in [1..npols] do
	    coord[i] +:= dt*mods[jj][i];
	end for;
	Append(~Gpart,<s,gg>);
	Append(~mods,coord);
    end for;
    return Gpart, mods;
end intrinsic;

intrinsic FullRepr_syz(F::SeqEnum,G::SeqEnum, GC::SeqEnum, Gz::SeqEnum)
	  -> SeqEnum
{ Compute a Gröbner basis of the module of syzygies of an ideal

Input:
- F sequence of polynomials
- G Sig-GB of the ideal generated by F, with coordinates GC
- Gz Sig-basis of syzygies of the ideal

Output:
- A Gröbner basis of the module of syzygies of the ideal
}
    syz := [];
    npols := #F;
    for j -> sz in Gz do
	cands := [];
	for jj in [1..#G] do
	    test, dt := IsDivisibleBy(sz,G[jj][1]);
	    if test then
		Append(~cands,<dt*LT(G[jj][2]),dt,jj>);
	    end if;
	end for;
	Sort(~cands);
	_,dt,jj := Explode(cands[1]);
	f := dt*G[jj][2];
	gg, coord := CollectCoordinates(sz,f,G,GC);
	assert gg eq 0;
	for i in [1..npols] do
	    coord[i] +:= dt*GC[jj][i];
	end for;
	Append(~syz,coord);
    end for;
    return syz;
end intrinsic;

intrinsic FullRepr_full(F::SeqEnum,Gs::SeqEnum,Gz::SeqEnum)
	  -> SeqEnum, SeqEnum, SeqEnum
{ Compute a Gröbner basis with coordinates and a Gröbner basis of the
module of syzygies }
    G,GC := FullRepr_GB(F,Gs);
    syz := FullRepr_syz(F,G,GC,Gz);
    return G,GC,syz;
end intrinsic;
    
intrinsic CoordinatesFromFullGB(f::RngMPolElt, G::SeqEnum, GC::SeqEnum) -> SeqEnum
{ Compute the coordinates of f in terms of the input polynomials

Input:
- f multivariate polynomial in the ideal <f1,...,fn>
- G GB or Sig-GB of <f1,...,fn>, with coordinates GC

Output:
- c1...cn such that f = c1 f1 + ... + cn fn
}
    if Type(GC[1]) eq SeqEnum then
	npols := #GC[1];
    elif Type(GC[1]) eq ModMPolElt then
	npols := Rank(Parent(GC[1]));
    else
	error "Unknown type for the list of coordinates";
    end if;
    if Type(G[1]) eq Tup then
	GG := [g[2] : g in G];
    elif Type(G[1]) eq RngMPolElt then
	GG := G;
    else
	error "Unknown type for the Gröbner basis";
    end if;
    P := Parent(f);
    fr, cG := NormalForm(f, [g[2] : g in G]);
    if fr ne 0 then
	error "The polynomial is not in the ideal.";
    end if;
    cF := [P!0 : i in [1..npols]];
    for i in [1..#cG] do
	if cG[i] ne 0 then
	    for j in [1..npols] do
		cF[j] +:= cG[i]*GC[i][j];
	    end for;
	end if;
    end for;
    return cF;
end intrinsic;
