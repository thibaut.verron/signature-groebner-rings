function sys_cyclic(n : order := "grevlex")
    // Cyclic-n system
    A := PolynomialRing(IntegerRing(),n,order);
    sys := [];
    for i in [1..n-1] do
	f := 0;
	for j in [1..n] do
	    term := &*[A.(k le n select k else k-n)
		       : k in [j..j+i-1]];
	    f +:= term;
	end for;
	Append(~sys,f);
    end for;
    Append(~sys,&*[A.k : k in [1..n]]-1);
    return sys;
end function;

function sys_katsura(n : order := "grevlex")
    // Katsura-n system
    A := PolynomialRing(IntegerRing(),n+1,order);
    if n le 6 then
	AssignNames(~A,Eltseq("xyztuvw")[1..n+1]);
    else
	AssignNames(~A,[Sprintf("u%o",i) : i in [1..n+1]]);
    end if;
    function varU (i)
	if i lt 0 then
            return varU(-i);
	elif i gt n then
            return 0;
	else
            return A.(i+1);
	end if;
    end function;
    K := [];
    for m in [-n+1..n-1] do
	new := &+([varU(l)*varU(m-l) : l in [-n..n]]) - varU(m);
	if not new in K then
            Append(~K,new);
	end if;
    end for;
    Append(~K,&+([varU(l) : l in [-n..n]]) - 1);
    return K;
end function;

// Systems from Eder-Pfister-Popescu 2017
P<x,y,z> := PolynomialRing(IntegerRing(),3,"grevlex");
gsys_EPP1 := [95*y^3, 54*x^2*y+33*x*y^2+26*x*z+91*z^2,55*x^3+69*y^2];
sys_EPP2 := [55*y^2*z+7*x^2+26*x*y,50*x^3+35*x^2*z+52*y^2,81*z^3];
sys_EPP3 := [14*x^3+57*x*y,72*y^3+12*x*y,27*x^2*z+43*y^2*z+69*z^2];
sys_EPP4 := [2*x^2*z+67*y^2*z+24*z^2,3*x^3,55*y^3+67*x^2+85*x*y];
sys_EPP5 := [352*x^2*y+670*y^3+273*y*z,718*x^3,961*y^2*z+283*x*z^2+723*y^2+401*y*z];
sys_EPP6 := [19*x*y^2+x^2*z+51*y^2*z+87*x*z^2+5*x^2+36*y^2,12*x^2*z,98*y*z^2+30*x*y+36*z^2];
sys_EPP7 := [507*x*y^2+308*x*z,108*x^3+801*y^3,197*x^2*z+382*y^2*z+689*x*z+464*z^2];
sys_EPP8 := [334*y*z^2,900*x^3+396*x*y^2+599*y^2,157*x^2*z+562*x*y*z+72*x*y+799*z^2];
sys_EPP9 := [556*x*y^2+362*y^2*z+141*y^2,295*x^2*y+303*x^2,335*x^3+590*x*y^2+541*z^2];
sys_EPP10 := [366*x*y^2+920*y^3,921*y^2*z+131*x*z^2+888*z^2,224*x^2*z+281*x*y+23*y*z];
sys_EPP11 := [97*x*y^2+647*y^3+715*z^2,694*x^2*y+65*x*y+536*y^2,85*y^3+267*x^2*z];
sys_EPP12 := [112*y^2*z+104*x^2+241*x*z,662*x^2*y+773*x*y^2,225*x^2*z+683*y^2*z+790*z^2];
sys_EPP13 := [887*x^3,793*x*y^2+589*x^2*z+474*x*z+366*y*z,265*x*y^2+20*x*z^2+449*y^2];
sys_EPP14 := [83*x^3+809*x^2*y+58*x*z,68*x*y^2+783*z^2,367*x^2*z+539*x*z^2+336*y*z];
sys_EPP15 := [306*x^2*z+330*x*z^2+611*z^2,572*x*y^2+494*y^2,361*x^2*y+95*x*y*z+578*x*z];
sys_EPP16 := [976*x^2*y+713*x^2*z+512*z^2,369*y^3+326*x^2*z,145*x*y^2+585*x*z+884*y*z];

/* Random systems */

function sys_random(P,degs,size)
    // P: polynomial ring
    // degs: list of degrees of the system
    // size: coefficients are taken between -size and size (inclusive)
    sys := [];
    dm := Max(degs);
    mons := [[P!1]];
    for i in [1..dm] do
	mons[i+1] := mons[i] cat Setseq(MonomialsOfDegree(P,i));
    end for;
    for d in degs do
	f := P!0;
	for m in mons[d+1] do
	    c := Random(-size,size);
	    f +:= c*m;
	end for;
	Append(~sys,f);
    end for;
    return sys;
end function;
