AttachSpec("spec");
load "tests/systems.m";

F := sys_cyclic(4);
P := Parent(F[1]);

nvars := Rank(P);
npols := #F;

/* Computation of signature Gröbner bases */

time G,Gs,Gz,cnt := GB_KandriRodyKapur(F);
/* G is a (reduced) GB, Gs a Sig-GB, Gz a Sig-basis of syzygies, cnt a report on */
/* the algorithm run */

/* Verify that G is a GB */
IsGroebner(G);
// true

/* Verify that the polynomials in Gs form a GB */
IsGroebner([g[2] : g in Gs]);
// true

/* We can do the same thing with Pan/Lichtblau's algorithm */

time G2,Gs2,Gz2,cnt2 := GB_Lichtblau(F);
IsGroebner(G2);
// true
IsGroebner([g[2] : g in Gs2]);
// true

/* Computation in the module */

Mod := EModule(P,npols,"pot");

/* Magma works with a different PoT ordering, so we need to reorder
the input elements */
FF := Reverse(F);

/* Basis with coefficients */
I := IdealWithFixedBasis(Reverse(F));
Gm := GroebnerBasis(I);

/* Test polynomial */

cc := sys_random(P,[2 : i in [1..#Gm]], 10);
ff := &+[cc[i]*Gm[i] : i in [1..#Gm]];

ccc := Coordinates(I,ff);
ff - &+[ccc[i]*FF[i] : i in [1..npols]];
// 0 (as expected)

/* Module of syzygies */
time M_magma := SyzygyMatrix(FF);
Syz_magma := sub<Mod | Rows(M_magma)>;

/* Now let's do those with signatures */
G,GC,syz := FullRepr_full(F,Gs,Gz);
/* G is a GB, G[i] has coordinates GC[i] in terms of F, syz is a basis
of the syzygy module*/

/* Reverse them to get coordinates in FF */
GC := [Reverse(g) : g in GC];
syz := [Reverse(s) : s in syz];

syz := [Mod!s : s in syz];
GC := [Mod!g : g in GC];
Syz := sub<Mod | syz>;

/* Check that we have the correct module of syzygies */
Syz eq Syz_magma;
// true

/* Double check that what we have are syzygies */
forall{s : s in syz | &+[s[i]*FF[i] : i in [1..npols]] eq 0};
// true

/* Check that we have a Gröbner basis of the module */
Groebner(Syz);
BB := Basis(Syz); // Groebner basis
forall{b : b in BB
       | exists{s : s in syz
		| IsDivisibleBy(LeadingTerm(b), LeadingTerm(s))}};
// true

/* Check that the GB with coordinates is a GB */
IsGroebner([g[2] : g in G]);
// true

/* Check that the coordinates of the GB elements are
correct */
forall{i : i in [1..#G] | &+[GC[i][j]*FF[j] : j in [1..npols]] eq G[i][2]};
// true

/* Check that the signatures are correct (but with indices reversed) */
forall{i : i in [1..#G]
       | Column(LeadingTerm(GC[i])) eq (npols+1 - G[i][1]`i)
	 and LeadingTerm(GC[i])[Column(LeadingTerm(GC[i]))]
	     eq G[i][1]`c * G[i][1]`m}; 
// true

/* And finally, check that we can compute the coordinates of a polynomial */
coords := CoordinatesFromFullGB(ff,G,GC);
ff - &+[coords[i]*FF[i] : i in [1..npols]];
// 0


