AttachSpec("spec");
load "tests/systems.m";

//F := sys_katsura(5);
F := sys_cyclic(6);

SetVerbose("MoellerSig",0);

print "GB (Magma)";
time _ := GroebnerBasis(F);

/* print "GB with coordinates (Magma)"; */
/* I := IdealWithFixedBasis(F); */
/* time Groebner(I); */

/* print "Syzygy basis (Magma)"; */
//time SyzM := SyzygyMatrix(F);

print "GB (Kandri-Rody/Kapur)";
time G,Gs,Gz,cnt1 := GB_KandriRodyKapur(F);

print "Full representation (KR/K)";
time GG,Mod,Syz1 := FullRepr_full(F,Gs,Gz);

print "GB (Pan/Lichtblau)";
time G,Gs,Gz,cnt2 := GB_Lichtblau(F);

print "Full representation (P/L)";
time GG,Mod,Syz2 := FullRepr_full(F,Gs,Gz);



/* Results:

==========================
Katsura-4:
GB (Magma)
Time: 0.180
GB with coordinates (Magma)
Time: 1.670
Syzygy basis (Magma)
Time: 1.630
GB (Kandri-Rody/Kapur)
Time: 1.250
Full representation (KR/K)
Time: 0.280
GB (Pan/Lichtblau)
Time: 1.590
Full representation (P/L)
Time: 0.270

Report on execution of KRK_pot
 - Total time:               1.350
 - Last signature:           7810*y*t^5*u^4*e5
 - Total considered elts:    420
 - Max length of the queue:  192
 - Total reduced elts:       188
   of which reductions to 0: 0
 - Cover criterion:          166
 - F5 + syzygy criteria:     208
 - Singular criterion:       12
 - Coprime criterion:        0
 - Chain criterion:          4549
   of which M criterion:     2633
            F criterion:     1020
            B criterion:     30
            G criterion:     866
 - Singular reduction crit.: 85

Report on execution of Lichtblau_pot
 - Total time:               1.600
 - Last signature:           7810*y*t^5*u^4*e5
 - Total considered elts:    855
 - Max length of the queue:  300
 - Total reduced elts:       412
   of which reductions to 0: 0
 - Cover criterion:          0
 - F5 + syzygy criteria:     448
 - Singular criterion:       0
 - Coprime criterion:        784
 - Chain criterion:          2518
   of which M criterion:     817
            F criterion:     848
            B criterion:     6
            G criterion:     847
 - Singular reduction crit.: 310

======================
Katsura 5

GB (Magma)
Time: 5.640
GB (Kandri-Rody/Kapur)
Time: 35.360
Full representation (KR/K)
Time: 5.810
GB (Pan/Lichtblau)
Time: 87.880
Full representation (P/L)
Time: 6.270

Report on execution of KRK_pot
 - Total time:               35.350
 - Last signature:           109891*x*t^2*u^5*v^6*e6
 - Total considered elts:    2048
 - Max length of the queue:  1087
 - Total reduced elts:       723
   of which reductions to 0: 0
 - Cover criterion:          1342
 - F5 + syzygy criteria:     915
 - Singular criterion:       221
 - Coprime criterion:        0
 - Chain criterion:          63263
   of which M criterion:     30924
            F criterion:     18944
            B criterion:     175
            G criterion:     13220
 - Singular reduction crit.: 324


Report on execution of Lichtblau_pot
 - Total time:               87.890
 - Last signature:           109891*x*t^2*u^5*v^6*e6
 - Total considered elts:    7178
 - Max length of the queue:  2170
 - Total reduced elts:       3983
   of which reductions to 0: 0
 - Cover criterion:          0
 - F5 + syzygy criteria:     3201
 - Singular criterion:       0
 - Coprime criterion:        4842
 - Chain criterion:          42581
   of which M criterion:     13561
            F criterion:     15321
            B criterion:     88
            G criterion:     13611
 - Singular reduction crit.: 3572


======================
Cyclic 4

GB (Magma)
Time: 0.000
GB with coordinates (Magma)
Time: 0.000
Syzygy basis (Magma)
Time: 0.000
GB (Kandri-Rody/Kapur)
Time: 0.020
Full representation (KR/K)
Time: 0.000
GB (Pan/Lichtblau)
Time: 0.010
Full representation (P/L)
Time: 0.000

======================
Cyclic 5

GB (Magma)
Time: 0.010
GB with coordinates (Magma)
Time: 954.550
Syzygy basis (Magma)
Time: 994.750
GB (Kandri-Rody/Kapur)
Time: 0.380
Full representation (KR/K)
Time: 0.060
GB (Pan/Lichtblau)
Time: 0.560
Full representation (P/L)
Time: 0.060

======================
Cyclic 6

GB (Magma)
Time: 0.010
GB (Kandri-Rody/Kapur)
Time: 238.950
Full representation (KR/K)
Time: 10.120
GB (Pan/Lichtblau)
Time: 727.650
Full representation (P/L)
Time: 14.000

Report on execution of KRK_pot
 - Total time:               238.940
 - Last signature:           -$.1*$.2*$.4*$.5^4*$.6^5*e6
 - Total considered elts:    3019
 - Max length of the queue:  1069
 - Total reduced elts:       742
   of which reductions to 0: 8
 - Cover criterion:          2384
 - F5 + syzygy criteria:     2078
 - Singular criterion:       5
 - Coprime criterion:        0
 - Chain criterion:          295510
   of which M criterion:     114943
            F criterion:     122748
            B criterion:     222
            G criterion:     57597
 - Singular reduction crit.: 8

Report on execution of Lichtblau_pot
 - Total time:               727.730
 - Last signature:           2*$.2*$.3^5*$.5^4*$.6^2*e6
 - Total considered elts:    9672
 - Max length of the queue:  2425
 - Total reduced elts:       5782
   of which reductions to 0: 8
 - Cover criterion:          0
 - F5 + syzygy criteria:     3896
 - Singular criterion:       0
 - Coprime criterion:        5698
 - Chain criterion:          301333
   of which M criterion:     48037
            F criterion:     175423
            B criterion:     15
            G criterion:     77858
 - Singular reduction crit.: 4883

 */
