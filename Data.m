/* Data types, etc */

/* Verbosity flags */

declare verbose GroebnerSig, 3;

/* Counters for pairs, reductions, etc */

declare type SigAlgoCounter;
declare attributes SigAlgoCounter: algo, tottime, lastsig,
	pairs, maxpairs, spol, reds, red0, syz,
	singred, singpol, cover, singpair,
	f5, coprime, chaintotal, chainM, chainB, chainG, chainF;

intrinsic _printfrec(c::SigAlgoCounter, field::MonStgElt, fmt::MonStgElt)
{ Replace the placeholder in fmt with c`field if assigned and print it.
Otherwise do nothing.

(internal function) }
    if assigned c``field then
	printf fmt, c``field;
    end if;
end intrinsic;

intrinsic Print(c::SigAlgoCounter)
{ Print the available values in the counter record c }
 
    _printfrec(c,"algo",       "Report on execution of %o\n");
    _printfrec(c,"tottime",    " - Total time:               %o\n");
    _printfrec(c,"lastsig",    " - Last signature:           %o\n");
    _printfrec(c,"pairs",      " - Total considered elts:    %o\n");
    _printfrec(c,"maxpairs",   " - Max length of the queue:  %o\n");
    _printfrec(c,"reds",       " - Total reduced elts:       %o\n");
    _printfrec(c,"red0",       "   of which reductions to 0: %o\n");
    _printfrec(c,"cover",      " - Cover criterion:          %o\n");
    _printfrec(c,"syz",        " - F5 + syzygy criteria:     %o\n");
    _printfrec(c,"singpol",    " - Singular criterion:       %o\n");
    _printfrec(c,"coprime",    " - Coprime criterion:        %o\n");

    if not assigned c`chainM then c`chainM := 0; end if;
    if not assigned c`chainF then c`chainF := 0; end if;
    if not assigned c`chainB then c`chainB := 0; end if;
    if not assigned c`chainG then c`chainG := 0; end if;
    c`chaintotal := c`chainM + c`chainF + c`chainB + c`chainG;
    
    _printfrec(c,"chaintotal", " - Chain criterion:          %o\n");
    _printfrec(c,"chainM",     "   of which M criterion:     %o\n");
    _printfrec(c,"chainF",     "            F criterion:     %o\n");
    _printfrec(c,"chainB",     "            B criterion:     %o\n");
    _printfrec(c,"chainG",     "            G criterion:     %o\n");
    _printfrec(c,"singred",    " - Singular reduction crit.: %o\n");
end intrinsic;
