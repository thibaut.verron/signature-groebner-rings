/* Various useful tools for the algorithms */

LC := LeadingCoefficient;
LM := LeadingMonomial;
LT := LeadingTerm;

/* Shortened printing */

intrinsic _terstr(f::RngMPolElt) -> MonStgElt
{ Short printable version of a polynomial term.

(internal function)}
    if LC(f) lt 1000 and LC(f) gt -1000 then
	return Sprintf("%o", LT(f));
    else
	return Sprintf("big*%o", LM(f));
    end if;
end intrinsic;

intrinsic _polstr(f::RngMPolElt) -> MonStgElt
{ Short printable version of a polynomial.

(internal function)}
    if #Coefficients(f) gt 1 then
	return Sprintf("%o+...", _terstr(LT(f)));
    else
	return _terstr(f);
    end if;
end intrinsic;

intrinsic _sigstr(s::Sig) -> MonStgElt
{ Short printable version of a signature.

(internal function)}
    c := s`c;
    if c lt 1000 and c gt -1000 then
	return Sprintf("%o*%o*e%o",s`c,s`m,s`i);
    else
	return Sprintf("big*%o*e%o",s`m,s`i);
    end if;
end intrinsic;

intrinsic _pairstr(sg::Tup)
	  -> MonStgElt
{ Short printable version of a pair.

(internal function)}
    return Sprintf("<%o,%o>",_sigstr(sg[1]),_terstr(LT(sg[2])));
end intrinsic;

intrinsic _discard(s::Sig,why::MonStgElt : doprint := false)
{ Hook run when discarding an element (for debug purposes)}
    if GetVerbose("GroebnerSig") ge 3 or doprint then
	printf "Discarding sig %o : %o\n",
	       _sigstr(s),why;
    end if;
    // Specific tests can be put here to investigate why a specific element
    // gets discarded.
end intrinsic;

/* Generic functions missing from Magma */

intrinsic SortKey(~L::SeqEnum[Tup], key::Program)
{ Sort routine by key instead of comparison function.

Requires only computing n keys instead of performing n log n comparisons
(2*n*log n key computations.) }
    K := [key(l) : l in L];
    ParallelSort(~K,~L);
end intrinsic;

intrinsic Quotrem_abs(a::RngIntElt,b::RngIntElt) -> RngIntElt, RngIntElt
{ Centered division }
    q,r := Quotrem(a,b);
    d := Floor(Abs(b)/2);
    if r ge Abs(b)-d then
	//print "too big";
	return q+Sign(b),r-Abs(b);
    elif r lt -d then
	//print "too small";
	return q-Sign(b),r+Abs(b);
    else
	return q,r;
    end if;
end intrinsic;
    
intrinsic Quotrem_abs(a::RngMPolElt,b::RngMPolElt) -> RngMPolElt, RngMPolElt
    { a = b*q+r with r small in absolute value}
    test, d := IsDivisibleBy(LM(a),LM(b));
    if not test then
	return 0,a;
    else
	q,r := Quotrem_abs(LC(a),LC(b));
	return q*d, r*LM(a);
    end if;
end intrinsic;

intrinsic Cmp(a::Any, b::Any) -> RngIntElt
    { Generic comparison function, suitable for e.g. sort }
    if a lt b then
	return -1;
    elif a gt b then
	return 1;
    else
	return 0;
    end if;
end intrinsic;


/* Replacements for bugged routines in Magma */

intrinsic ReduceGroebnerBasis_Z(G::SeqEnum) -> SeqEnum
{ Reduce the Groebner basis G.

Magma's implementation is not correct for Z, the LCs can become bigger.

It should only happen when there are negative coefficients, so we just normalize those.}
    if #G eq 0 then
	return [];
    end if;
    for i->g in G do
	if LC(g) lt 0 then G[i] *:= -1; end if;
    end for;
    return ReduceGroebnerBasis(G);
end intrinsic;

intrinsic SPolynomial_Z(f::RngMPolElt, g::RngMPolElt) -> RngMPolElt
{ Return the S-polynomial of f and g.

Magma's implementation is not correct for Z, as it computes the product of the
leading terms instead of their LCM. }
    tf := LT(f);
    tg := LT(g);
    tfg := Lcm(tf,tg);
    return (tfg div tf)*f - (tfg div tg)*g;
end intrinsic;
