/* Data types and operations for signatures

Signatures are records with the following mandatory fields:
- i : integer, the signature index
- c : ring element, the coefficient
- m : multivariate polynomial, the monomial

Additional fields:
- key_strict : sorting key, equivalent to strict comparison of the
  signatures
- note : any

Main type: Sig
Subtypes:
- SigPot (position over term)
- SigTop (term over position)
- SigDPol (degree over position over term)
- SigNoSig (dummy type for ignored signatures in incremental algorithms
  -- experimental)
*/

LT := LeadingTerm;
LM := LeadingMonomial;
LC := LeadingCoefficient;

declare type Sig;
declare attributes Sig: i, c, m, key_strict, note;

declare type SigPot : Sig;
declare type SigTop : Sig;
declare type SigDPot : Sig;
declare type SigNoSig : Sig; 
declare attributes SigDPot: d;

/* Creation */

intrinsic SigNew(type::Cat, i::RngIntElt, t::RngMPolElt : d := 0) -> Sig
{ Create the signature t*e[i] of type type.

  Optional argument:
  d: degree of the element (for SigDPot)}
    return SigNew(type, i, LeadingCoefficient(t),LeadingMonomial(t)
		  : d := d);
end intrinsic;

intrinsic SigNew(type:: Cat, i::RngIntElt, c :: RngElt, m :: RngMPolElt
		 : d := 0) -> Sig
{ Create the signature c*m*e[i] of type type.

  Optional argument:
  d: degree of the element (for SigDPot) }
    s := New(type);
    s`i := i;
    s`c := c;
    s`m := m;
    if type eq SigDPot then
	s`d := d;
    end if;
    // SigGenKeys(~s);
    return s;
end intrinsic;

intrinsic SigNull(type::Cat) -> Sig
{ Create a zero signature of type type. }
    s := New(type);
    return s;
end intrinsic;

intrinsic SigCopy(s::Sig) -> Sig
{ Copy the signature s. }
    ss := New(Type(s));
    ss`i := s`i;
    ss`c := s`c;
    ss`m := s`m;
    if Type(s) eq SigDPot then
	ss`d := s`d;
    end if;
    return ss;
end intrinsic;


/* Printing */

intrinsic Print(s::Sig, level::MonStgElt)
{ Print the signature s. The printing level is currently ignored. }
    if assigned s`i then
	printf "%o*e%o", s`c*s`m, s`i;
    else
	printf "0";
    end if;
end intrinsic;

intrinsic Print(s::SigNoSig, level::MonStgElt)
{ Print the dummy signature s. The printing level is currently ignored. }
    printf "e%o", s`i;
end intrinsic;


/* Accessing */

intrinsic PolRing(s::Sig) -> RngMPol
{ Return the polynomial ring associated to the signature s. }
    return Parent(s`m);
end intrinsic;

// BUG: The name Explode leads to a segfault
intrinsic SigExplode(s::Sig) -> RngIntElt, RngElt, RngMPolElt
{ Return the index, coefficient and monomial of the signature s. }
    return s`i, s`c, s`m;
end intrinsic;

intrinsic SigIsNull(s::Sig) -> Sig
{ Tests whether the signature is the zero signature (as created
by SigNull )}
    return not assigned s`i;
end intrinsic;

/* Maintenance of the sorting keys */

intrinsic SigGenKey(~s::SigPot)
{ Generate the sorting key for the PoT signature s. }
    s`key_strict := <s`i, s`m>;
end intrinsic;

intrinsic SigGenKey(~s::SigTop)
{ Generate the sorting key for the ToP signature s. }
    s`key_strict := <s`m, s`i>;
end intrinsic;

intrinsic SigGenKey(~s::SigDPot)
{ Generate the sorting key for the D-PoT signature s. }
    s`key_strict := <s`d, s`i, s`m>;
end intrinsic;

intrinsic SigGenKey(~s::SigNoSig)
{ Generate the sorting key for the dummy signature s. }
    s`key_strict := <s`i>;
end intrinsic;

intrinsic SigKey(s::Sig) -> Tup
{ Access the sorting key of the signature s. Generate a key if
  s does not already have one }
    if not assigned s`key_strict then
	SigGenKey(~s);
    end if;
    return s`key_strict;
end intrinsic;
    
intrinsic SigKeyMagnitude(s::Sig) -> Tup
{ Generate a key taking into account the magnitude of the
  coefficient of s. }
    return Append(SigKey(s),Abs(s`c));
end intrinsic;

/* Comparison of non-dummy signatures*/

intrinsic 'eq' (s1::Sig, s2::Sig) -> Bool
{ }
    return (SigIsNull(s1) and SigIsNull(s2))
	   or ((not SigIsNull(s1))
	       and (not SigIsNull(s2))
	       and s1`i eq s2`i and s1`m eq s2`m and s1`c eq s2`c);
end intrinsic;

intrinsic 'lt' (s1::Sig, s2::Sig) -> Bool
{ }
    if not assigned s1`key_strict then
	SigGenKey(~s1);
    end if;
    if not assigned s2`key_strict then
	SigGenKey(~s2);
    end if;
    return s1`key_strict lt s2`key_strict;
end intrinsic;

intrinsic 'le' (s1::Sig, s2::Sig) -> Bool
{ }
    if not assigned s1`key_strict then
	SigGenKey(~s1);
    end if;
    if not assigned s2`key_strict then
	SigGenKey(~s2);
    end if;
    return s1`key_strict le s2`key_strict;
end intrinsic;

intrinsic 'gt' (s1::Sig, s2::Sig) -> Bool
{ }
    return s2 lt s1;
end intrinsic;

intrinsic 'ge' (s1::Sig, s2::Sig) -> Bool
{ }
    return s2 le s1;
end intrinsic;

intrinsic Simeq(s1::Sig, s2::Sig) -> Bool
{ Return true if and only if s1 and s2 are equal up to coefficient. }
    return s1`i eq s2`i and s1`m eq s2`m;
end intrinsic;

intrinsic SigEqSign(s1::Sig, s2::Sig) -> Bool
{ Return true if and only if s1 and s2 are equal up to sign }
    return s1`i eq s2`i
	   and s1`m eq s2`m
	   and (s1`c eq s2`c or s1`c eq -s2`c);
end intrinsic;

intrinsic SigCmpProd(s1::Sig, t1::RngMPolElt, s2::Sig, t2::RngMPolElt)
	  -> RngIntElt
{ Return:
  -1 if t1*s1 lt t2*s2
  0 if t1*s1 = t2*s2 (up to coefficient)
  1 if t1*s1 gt t2*s2
}
    return Cmp(t1*s1, t2*s2);
end intrinsic;

intrinsic SigCmpProd(s1::Sig, t1::RngMPolElt, s2::Sig)
	  -> RngIntElt
{ Return:
  -1 if t1*s1 lt s2
  0 if t1*s1 = s2 (up to coefficient)
  1 if t1*s1 gt s2}
    return Cmp(t1*s1, s2);
end intrinsic;

intrinsic SigCmpProd(s1::SigPot, t1::RngMPolElt, s2::SigPot, t2::RngMPolElt)
	  -> RngIntElt
{ Return:
  -1 if t1*s1 lt t2*s2
  0 if t1*s1 = t2*s2 (up to coefficient)
  1 if t1*s1 gt t2*s2

Slight optimisation for PoT signatures}
    if s1`i lt s2`i then
	return -1;
    elif s1`i gt s2`i then
	return 1;
    else
	return Cmp(LM(t1)*s1`m, LM(t2)*s2`m);
    end if;
end intrinsic;

intrinsic SigCmpProd(s1::SigPot, t1::RngMPolElt, s2::SigPot)
	  -> RngIntElt
{ Return:
  -1 if t1*s1 lt s2
  0 if t1*s1 = s2 (up to coefficient)
  1 if t1*s1 gt s2

Slight optimisation for PoT signatures }
    if s1`i lt s2`i then
	return -1;
    elif s1`i gt s2`i then
	return 1;
    else
	return Cmp(LM(t1)*s1`m, s2`m);
    end if;
end intrinsic;



/* Comparison of dummy signatures */

intrinsic 'eq' (s1::SigNoSig, s2::SigNoSig) -> Bool
{ Equality test of dummy signatures: false by definition }
    return false;
end intrinsic;

intrinsic 'lt' (s1::SigNoSig, s2::SigNoSig) -> Bool
{ Comparison of dummy signatures: only uses the index }
    return s1`i lt s2`i;
end intrinsic;

intrinsic 'le' (s1::SigNoSig, s2::SigNoSig) -> Bool
{ Comparison of dummy signatures: only uses the index }
    return s1`i le s2`i;
end intrinsic;

intrinsic Simeq(s1::SigNoSig, s2::SigNoSig) -> Bool
{ Almost-equality test of dummy signatures: false by definition}
    return false;
end intrinsic;

intrinsic SigEqSign(s1::SigNoSig, s2::SigNoSig) -> Bool
{ Equality up to sign test for dummy signatures: false by definition}
    return false;
end intrinsic;

intrinsic SigCmpProd(s1::SigNoSig, t1::RngMPolElt, s2::SigNoSig, t2::RngMPolElt)
	  -> RngIntElt
{ Comparison with product for dummy signatures.

Return -1 by convention

WARNING: The validity of this output depends on the expectations of the
caller (Swap the arguments if necessary)
 }	 
    return -1;
end intrinsic;

intrinsic SigCmpProd(s1::SigNoSig, t1::RngMPolElt, s2::SigNoSig)
	  -> RngIntElt
{ Comparison with product for dummy signatures.

Return -1 by convention

WARNING: The validity of this output depends on the expectations of the
caller (Swap the arguments if necessary)
}
    return -1;
end intrinsic;

/* Operations : multiplication */

intrinsic '*:=' (~s::Sig, t::RngMPolElt)
{ Multiply a signature by a term, in place. }
    s`c *:= LeadingCoefficient(t);
    s`m *:= t ne 0 select LeadingMonomial(t) else 0;
    if Type(s) eq SigDPot then
	s`d +:= Degree(t);
    end if;
    delete s`key_strict;
end intrinsic;

intrinsic '*' (t :: RngMPolElt, s :: Sig) -> Sig
{ Multiply a signature by a term. }
    ss := SigCopy(s);
    ss *:= t;
    return ss;
end intrinsic;

/* Operations: addition */

intrinsic '+'(s1::Sig, s2::Sig) -> Sig
{ Add two signatures. The result is 0 if the two signatures cancel out
 (singular addition)}
    case Cmp(s1,s2):
    when -1:
	return s2;
    when 1:
	return s1;
    else
	sc := s1`c + s2`c;
	if sc eq 0 then
	    return SigNull(Type(s1));
	else
	    return SigNew(Type(s1),s1`i,sc,s1`m);
	end if;
    end case;
end intrinsic;

intrinsic '+'(s1::SigNoSig, s2::SigNoSig) -> SigNoSig
{ Add two dummy signatures. The result is never 0.}
    return s1 lt s2 select s2 else s1;
end intrinsic;

intrinsic '-'(s1::Sig, s2::Sig) -> Sig
{ Subtract two signatures. The result is 0 if the two signatures
 cancel out (singular subtraction). }
    return s1 + PolRing(s1)!(-1)*s2;
end intrinsic;

intrinsic SigAdd (s1::Sig, s2::Sig : strict := true) -> Sig
{ Add two signatures.

If strict, return 0 if the addition is not regular.

If strict is false, return 0 if the addition is singular (equivalent
to '+'). }
    if not strict then
	return s1+s2;
    elif s1 lt s2 then
	return s2;
    elif s1 gt s2 then
	return s1;
    else
	return SigNull(Type(s1));
    end if;
end intrinsic;

intrinsic SigAdd (s1::SigNoSig, s2::SigNoSig : strict := true) -> SigNoSig
{ Addition of dummy signatures, equivalent to '+' }
    return s1+s2;
end intrinsic;

/* Operation: division */

intrinsic IsDivisibleBy(s1::Sig, s2::Sig) -> Bool, RngMPolElt
{ If s1 is divisible by s2, return true and the quotient. Else return false. }
    if s1`i ne s2`i then
	return false,0;
    else
	return IsDivisibleBy(s1`c*s1`m, s2`c*s2`m);
    end if;
end intrinsic;

intrinsic IsDivisibleBy(s1::SigNoSig, s2::SigNoSig) -> Bool, RngMPolElt
{ Divisibility test of dummy signatures, returns false by convention

WARNING: the validity of this result depends on the expectations of the
caller. }
    return false, 0;
end intrinsic;

intrinsic _Quorem(s1::Sig, s2::Sig) -> RngMPolElt
{ Division with remainder in place for signatures.

After the call, s1 contains the remainder of the division of s1 by s2,
and the quotient is returned.

(internal function)}
    if s1`i ne s2`i then
	return 0;
    else
	q,r := Quotrem_abs(s1`c*s1`m, s2`c*s2`m);
	if q ne 0 then
	    s1`c := LeadingCoefficient(r);
	end if;
	return q;
    end if;
end intrinsic;

intrinsic _Quorem(s1::SigNoSig, s2::SigNoSig) -> RngMPolElt
{ Division with remainder for dummy signatures

(internal function)}
    return 0;
end intrinsic;

intrinsic Quorem(s1::Sig, s2::Sig) -> RngMPolElt, Sig
{ Division with remainder for signatures.

Return q, r such that s1 = q*s2 + r.
If c is the coefficient of s2, the coefficient of r is in the interval
[-floor(c/2), floor(c/2)]. }
    ss1 := SigCopy(s1);
    q := _Quorem(ss1,s2);
    return q,ss1;
end intrinsic;

intrinsic 'div' (s1::Sig, s2::Sig) -> RngMPolElt
{ }
    q,_ := Quorem(s1,s2);
    return q;
end intrinsic;

intrinsic 'mod' (s1::Sig, s2::Sig) -> Sig
{ }
    _,r := Quorem(s1,s2);
    return r;
end intrinsic;

intrinsic 'mod:=' (~s1::Sig, s2::Sig)
{ }
    _ := _Quorem(s1,s2);
end intrinsic;
