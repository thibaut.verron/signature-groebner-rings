/* Algorithms */

LT := LeadingTerm;
LC := LeadingCoefficient;
LM := LeadingMonomial;

/* Subroutines for Moeller's algorithm */

intrinsic pair_sorting_key_Moeller(p::Tup) -> Tup
{ Key to use for sorting the pairs in the queue in Moeller's algorithm.

(experimental, subject to change)}
    return <SigKeyMagnitude(p[3]),LeadingTerm(p[4])>;//,LeadingMonomial(p[4]),p[7],Abs(p[8])>;
end intrinsic;


intrinsic update_pairs_Moeller(~queue::SeqEnum[Tup],
		       ~lcm_cache::SeqEnum[SeqEnum],
		       sf::Tup,
		       ~SGw::SeqEnum[Tup],
		       ~cnt::SigAlgoCounter)
{ Update the list of pairs for Moeller's algorithm.

queue: list of pairs
lcm_cache: cache of the lcmlt(gi,gj)
sf: tuple <s,f> representing the new element f with signature s
G: current sig weak Gröbner basis
cnt: counter 
}
    s,f := Explode(sf);
    ltf := LT(f);
    Append(~lcm_cache,[]);
    r := #SGw + 1;
    cands := [];
    for i->sg in SGw do
	sfg,tfg,dt1,dt2,k := SPair(sg[1],LT(sg[2]),s,ltf);
	Append(~lcm_cache[r],tfg);
	if k eq 0 then
	    cnt`coprime +:= 1;
	elif k eq 2 then
	    cnt`singpair +:= 1;
	else
	    Append(~cands,<i,sfg,tfg,dt1,dt2>);
	end if;
    end for;
    Append(~lcm_cache[r],ltf);

    for cand in cands do
	i,sfg,tfg,dt1,dt2 := Explode(cand);
	// Gebauer-Moeller forward
	if exists{true : j in [1..i-1]
		 | chain_criterion_F(<lcm_cache[i][j],lcm_cache[r][i],lcm_cache[r][j]>,
				     sfg,LT(SGw[j][2]),SGw[j][1])} then
	    cnt`chainF +:= 1;
	elif exists{true: j in [1..i-1]
		    | chain_criterion_M(
			      <lcm_cache[i][j],lcm_cache[r][i],lcm_cache[r][j]>,
			      sfg,LT(SGw[j][2]),SGw[j][1])}
	     or exists{true: j in [i+1..r-1]
		      | chain_criterion_M(
				    <lcm_cache[j][i],lcm_cache[r][i],lcm_cache[r][j]>,
				    sfg,LT(SGw[j][2]),SGw[j][1])} then
	    cnt`chainM +:= 1;
	else
	    pair := <i,r,sfg,tfg,dt1,dt2>;
	    Append(~pair,pair_sorting_key_Moeller(pair));
	    Append(~queue,pair);
	end if;
    end for;
    // Gebauer-Moeller backward
    to_remove := [];
    for kk->pp in queue do
       	i,j,sij,tij,_ := Explode(pp);
       	if i ne 0 and j ne r
	   and chain_criterion_B(<lcm_cache[j][i],lcm_cache[r][i],lcm_cache[r][j]>,
				 sij,ltf,s) then
	    Append(~to_remove,kk);
    	end if;
    end for;
    // Remove from the back to avoid having later indices shift.
    Reverse(~to_remove);
    for i in to_remove do
	cnt`chainB +:= 1;
    	Remove(~queue,i);
    end for;

    /* Sort first by sig, then by t-deg, then by lc(sig) */
    
    /* SortKey(~queue, func<p | Sig_key_magnitude(p[3])>); */
    //SortKey(~queue, pair_sorting_key);
    
    /* FIXME: If it was the first coordinate there would be no need for a custom
    function */
    SortKey(~queue, func<p | p[7]>);
    Reverse(~queue);
end intrinsic;


intrinsic red_sorting_key (sg::Tup) -> Tup
{ Key to use to sort elements in the strong Gröbner basis.

(experimental, subject to change)}
    return <#Terms(sg[2]),SigKey(sg[1])>;
end intrinsic;


intrinsic can_eliminate_SGB(old::Tup, new::Tup)
	  -> BoolElt
{ Return true if old, as an element of the strong GB, makes it
      useless to add new }
    /* Note: elements of SGs have only a monomial
    in their signature. They are not used for super
    reductions. */
    test, ds := IsDivisibleBy(new[2],old[2]);
    res := false;
    if test then
	ds := LM(ds);
	if SigCmpProd(old[1],ds,new[1]) le 0 then
	    res := true;
	end if;
    end if;
    /* printf "old=%o, new=%o, res=%o\n", old,new,res; */
    return res;
end intrinsic;

intrinsic update_SGB_batch(~SGs::SeqEnum[Tup], ~SGw::SeqEnum[Tup],
			   ~Grem::SetEnum[RngIntElt],
			   s::Sig)
{ Update the strong GB with all elements having signature \\simeq s in SGw.

By grouping the GCD computations in this way, we could in the future be
able to do a smaller number of polynomial multiplications than by computing
the GCD one by one as they pop.}
    tstart := Realtime();
    
    // Elements to add
    new := [i : i in [1..#SGw] | Simeq(SGw[i][1],s)];
    nnew := #new;
    ss := SigCopy(s);
    ss`c := 1;

    for n in new do
    	update_SGB(~SGs,ss,~SGw[n][2]);
    end for;

    // Grem is not used for Moeller at the moment
    for i in [#Grem+1..#SGs] do Include(~Grem, i); end for;
    
    SortKey(~SGs, red_sorting_key);
    
    return;

end intrinsic;
    

intrinsic update_SGB(~SGs::SeqEnum[Tup], s::Sig, ~f::RngMPolElt : true_sig := false)
{ Update the strong Gröbner basis with the new element (s,f).

If true_sig is true, also compute the coefficient part of the G-polynomials (and ensure that this coefficient part is non-zero).}
    if LC(f) lt 0 then f := -f; end if;
    ssf := <s,f>;
    cand := [ssf];
    Pol := Parent(f);
    for i in [1..#SGs] do
	sg := SGs[i];
	p := <>;
	GPair(~sg,~ssf,~p : true_sig := true_sig);
	/* if #p ne 0 and LT(p[2]) eq 16*Pol.4^3*Pol.5^2 then */
	/*     error("debug here"); */
	/* end if; */
	if #p ne 0 then
	    Append(~cand,p);
	end if;
    end for;
    // Note: we can detect whether the new pol is useful just with the
    // lt
    for p in cand do
	pp := <p[1],LT(p[2])>;
	fnd := false;
	to_remove := [];
	for i->pi in SGs do
	    ppi := <pi[1],LT(pi[2])>;
	    // printf "%o, %o, %o\n", i, pp, ppi;
	    if can_eliminate_SGB(ppi,pp) then
		/* vprintf GroebnerSig,3: "SGB: not adding\n"; */
		fnd := true;
		break;
	    elif can_eliminate_SGB(pp,ppi) then
		/* vprintf GroebnerSig,3: "SGB: replacing with new elt\n"; */
		Append(~to_remove,i);
	    end if;
	end for;
	if not fnd then
	    /* vprintf GroebnerSig,3: "SGB: appending new elt\n"; */
	    Append(~SGs,p);
	end if;
	Reverse(~to_remove);
	for i in to_remove do
	    Remove(~SGs,i);
	end for;
    end for;
    
    //SortKey(~SGs, red_sorting_key);
    //Reverse(~SGs);
end intrinsic;

/* Subroutines for Kandri-Rody and Kapur's algorithm */

intrinsic update_pairs_KRK(~queue::SeqEnum[Tup],
			   ~lcm_cache::SeqEnum[SeqEnum],
			   sf::Tup,
			   ~G::SeqEnum[Tup],
			   ~Grem::SetEnum[RngIntElt],
			   ~cnt::SigAlgoCounter)
{ Update the list of pairs for Kandri-Rody and Kapur's algorithm.

queue: list of pairs
lcm_cache: cache of the lcmlt(gi,gj)
sf: tuple <s,f> representing the new element f with signature s
G: current sig Gröbner basis
Grem: set of indices of elements not removed from the basis
cnt: counter }
    s,f := Explode(sf);
    r := #G+1;
    Append(~lcm_cache,[]);
    candsG := [];
    candsS := [];
    ltf := LT(f);
    divtest := 0;
    for i in [1..#G] do
	if not i in Grem then
	    Append(~lcm_cache[r], <0,0>);
	    continue;
	end if;
	spg := G[i];
	sg, pg := Explode(spg);
	for forcetype in ["S","G"] do
	    sfg, tdeg, dt1,dt2, type, c := SGPair(sg,pg,s,f
						  : force:=forcetype);
	    if type eq "S" then
		Append(~lcm_cache[r], <tdeg,divtest>);
	    end if;
	    if SigIsNull(sfg) then
		// Singular S-polynomial
		continue;
	    end if;
	    pair := <i,r,sfg,tdeg,dt1,dt2,type,c>;
	    if type eq "S" then
		Append(~candsS, pair);
	    else
		Append(~candsG, pair);
	    end if;
	end for;
    end for;

    //existing := {<p[3],p[4]*p[8]> : p in queue};
    
    to_remove := [];
    for ii in [1..#candsS] do
	cand := candsS[ii];
	i,_,sfg,tfg,dt1,dt2,_ := Explode(cand);
	// Criteria
	if dt1 eq LT(G[i][2]) or -dt1 eq LT(G[i][2]) then
	    // Buchberger coprime
	    cnt`coprime +:= 1;
	    _discard(sfg,"(pair) buchberger");
	    continue;
	else
	    /* if <sfg,tfg> in existing then */
	    /* 	continue; */
	    /* end if; */
	    if exists{true: j in Grem
		      | j lt i and 
			chain_criterion_F(<lcm_cache[i][j][1],
					   lcm_cache[r][i][1],
					   lcm_cache[r][j][1]>,
					  sfg,LT(G[j][2]),G[j][1])
		     }
		     // F(i,r;j)
		then
		// Gebauer-Moeller forward
		cnt`chainF +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller F (current)");
		continue;
	    elif exists{true: j in Grem
			| j lt i and
			  chain_criterion_M(
			      <lcm_cache[i][j][1],
			       lcm_cache[r][i][1],
			       lcm_cache[r][j][1]>,
			      sfg,LM(G[j][2]),G[j][1])}
		 or exists{true: j in Grem
			| j gt i and
			  chain_criterion_M(
				  <lcm_cache[j][i][1],
				   lcm_cache[r][i][1],
				   lcm_cache[r][j][1]>,
				  sfg,LT(G[j][2]),G[j][1]) // M(i,r;j)
		       } then
		cnt`chainM +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller M (current)");
		continue;
	    else
		Append(~cand,pair_sorting_key(cand));
		//Include(~existing,<sfg,tfg>);
		Append(~queue,cand);
	    end if;
	end if;
    end for;

    // G-polynomials
    for ii in [1..#candsG] do
	cand := candsG[ii];
	i,_,sfg,tfg,dt1,dt2,_,c := Explode(cand);
	/* if <sfg,c*tfg> in existing then */
	/*     continue; */
	/* end if; */
	if exists{true: j in Grem
		  | /* G[j][2] ne 0 and */
		  chain_criterion_G(c*LM(lcm_cache[r][i][1]),
				    LT(G[j][2]),
				    sfg,
				    G[j][1]
				    : strict := false)}
	    then
	    cnt`chainG +:= 1;
	    _discard(sfg,"(pair) Gebauer-Moeller G (current)");
	    continue;
	end if;
	// If same sig-lead ratio as i, remove from G
	/* if c*tfg*G[i][1] eq LT(G[i][2])*sfg
	   and LM(tfg) eq LM(G[i][2]) then */
	/*     remove_from_G(i,~G,~Grem,~queue,~to_remove,
	       "same sig-lead ratio"); */
	/* end if; */
	Append(~cand,pair_sorting_key(cand));
	//Include(~existing,<sfg,c*tfg>);
	Append(~queue,cand);
    end for;

    // Gebauer-Moeller, backward
    for kk->pp in queue do
	i,j,sij,tij,_,_,type,c,_ := Explode(pp);
	if i ne 0 and j ne r then
	    if type eq "S" //and j ne r 
	       and chain_criterion_B(<lcm_cache[j][i][1],
				      lcm_cache[r][i][1],
				      lcm_cache[r][j][1]>,
				     sij,ltf,s)
       	        then
		cnt`chainB +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller B (back)");
		Append(~to_remove,kk);
	    /* elif type eq "S" and isCoveredBy(sij,tij,s,ltf) then */
	    /* 	cnt`cover +:= 1; */
	    /* 	_discard(sfg,"(pair) cover (back)"); */
	    /* 	Append(~to_remove,kk); */
	    elif type eq "G"
		 and chain_criterion_G(c*LM(lcm_cache[j][i][1]),LT(f),
				       sij,s) then
		Append(~to_remove,kk);
		cnt`chainG +:= 1;
		_discard(sij,"(pair) Gebauer-Moeller G (back)");
	    end if;
	end if;
    end for;

    Sort(~to_remove);
    Reverse(~to_remove);
    for i in to_remove do
	Remove(~queue,i);
    end for;

    sort_queue(~queue);

end intrinsic;



/* Subroutines for Lichtblau's algorithm */

intrinsic update_pairs_Lichtblau(~queue::SeqEnum[Tup],
				 ~lcm_cache::SeqEnum[SeqEnum],
				 sf::Tup,
				 ~G::SeqEnum[Tup],
				 ~Grem::SetEnum[RngIntElt],
				 ~cnt::SigAlgoCounter)
{ Update the list of pairs for Lichtblau's algorithm.

queue: list of pairs
lcm_cache: cache of the lcmlm(gi,gj)
sf: tuple <s,f> representing the new element f with signature s
G: current sig Gröbner basis
Grem: set of indices of elements not removed from the basis
cnt: counter }
    s,f := Explode(sf);
    r := #G+1;
    Append(~lcm_cache,[]);
    candsG := [];
    candsS := [];
    lmf := LM(f);
    for i in [1..#G] do
	if not i in Grem then
	    Append(~lcm_cache[r], <0,0>);
	    continue;
	end if;
	spg := G[i];
	sg, pg := Explode(spg);
	sfg, tdeg, dt1,dt2, type, c := SGPair(sg,pg,s,f);
	if type eq "G" then 
	    divtest := 0;
	elif Abs(LC(dt1)) eq 1 then
	    divtest := -1; // C[r] divides C[i]
	else
	    divtest := 1;
	end if;
	Append(~lcm_cache[r], <tdeg,divtest>);
	assert Type(s) eq SigNoSig or dt1*sg + dt2*s eq sfg;
	if SigIsNull(sfg) then
	    // Singular S-polynomial
	    continue;
	end if;
	pair := <i,r,sfg,tdeg,dt1,dt2,type,c>;
	if type eq "S" then
	    Append(~candsS, pair);
	else
	    Append(~candsG, pair);
	end if;
    end for;

    for ii in [1..#candsS] do
	cand := candsS[ii];
	i,_,sfg,tfg,dt1,dt2,type,_ := Explode(cand);
	assert type eq "S";
	// Criteria
	if (Degree(tfg) eq Degree(LM(f)) + Degree(LM(G[i][2]))) then
	    // Buchberger coprime
	    cnt`coprime +:= 1;
	    _discard(sfg,"(pair) buchberger");
	/* _discard(sfg,Sprintf("(pair) buchberger: %o %o %o %o %o",LT(f),LT(G[i][2]),dt1,dt2,type)); */
	else
	    lc_crit := [j : j in Grem
			| j ne i
			  and chain_criterion_lc_with_13(
				      j lt i select lcm_cache[i][j][2]
				      else -lcm_cache[j][i][2],
				      LC(G[i][2]),
				      LC(f),
				      LC(G[j][2]))];
	    if exists{true: j in lc_crit
		      | j lt i and 
			chain_criterion_F(<lcm_cache[i][j][1],
					   lcm_cache[r][i][1],
					   lcm_cache[r][j][1]>,
					  sfg,LM(G[j][2]),G[j][1])
		     }
		     // F(i,r;j)
		then
		// Gebauer-Moeller forward
		cnt`chainF +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller F (current)");
		continue;
	    elif exists{true: j in lc_crit
			| j lt i and
			  chain_criterion_M(
			      <lcm_cache[i][j][1],
			       lcm_cache[r][i][1],
			       lcm_cache[r][j][1]>,
			      sfg,LM(G[j][2]),G[j][1])}
		 or exists{true: j in lc_crit
			    | j gt i and
			      chain_criterion_M(
				      <lcm_cache[j][i][1],
				       lcm_cache[r][i][1],
				       lcm_cache[r][j][1]>,
				      sfg,LM(G[j][2]),G[j][1]) // M(i,r;j)
			   } then
		cnt`chainM +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller M (current)");
		continue;
	    else
		Append(~cand,pair_sorting_key(cand));
		Append(~queue,cand);
	    end if;
	end if;
    end for;

    to_remove := [];
    // G-polynomials
    for ii in [1..#candsG] do
	cand := candsG[ii];
	i,_,sfg,tfg,dt1,dt2,_,c := Explode(cand);
	if exists{true: j in Grem
		  | /* G[j][2] ne 0 and */
		  chain_criterion_G(c*lcm_cache[r][i][1],
				    LT(G[j][2]),
				    sfg,
				    G[j][1])}
	    then
	    cnt`chainG +:= 1;
	    _discard(sfg,"(pair) Gebauer-Moeller G (current)");
	    continue;
	end if;
	// If same sig-lead ratio as i, remove from G
	/* if c*tfg*G[i][1] eq LT(G[i][2])*sfg and tfg eq LM(G[i][2]) then */
	/*     remove_from_G(i,~G,~queue,~to_remove); */
	/* end if; */
	Append(~cand,pair_sorting_key(cand));
	Append(~queue,cand);
    end for;

    // Gebauer-Moeller, backward
    for kk->pp in queue do
	i,j,sij,tij,_,_,type,c,_ := Explode(pp);
	if i ne 0 and j ne r then
	    if type eq "S" //and j ne r 
	       and chain_criterion_B(<lcm_cache[j][i][1],
				      lcm_cache[r][i][1],
				      lcm_cache[r][j][1]>,
				     sij,lmf,s)
       	       and chain_criterion_lc_with_12(
			   lcm_cache[j][i][2],
			   LC(G[i][2]),LC(G[j][2]),LC(f)) then
		cnt`chainB +:= 1;
		_discard(sfg,"(pair) Gebauer-Moeller B (back)");
		Append(~to_remove,kk);
	    elif type eq "G"
		 and chain_criterion_G(c*lcm_cache[j][i][1],LT(f),
				       sij,s) then
		Append(~to_remove,kk);
		cnt`chainG +:= 1;
		_discard(sij,"(pair) Gebauer-Moeller G (back)");
	    end if;
	end if;
    end for;

    Sort(~to_remove);
    Reverse(~to_remove);
    for i in to_remove do
	Remove(~queue,i);
    end for;

    sort_queue(~queue);
end intrinsic;

/* General subroutines for both Lichtblau's and Kandri-Rody and Kapur's algorithms */

intrinsic pair_sorting_key(p::Tup) -> Tup
{ Key to use for sorting the pairs in the queue.

(experimental, subject to change)}
    return <SigKeyMagnitude(p[3]),LeadingTerm(p[4]),p[8]>;//,LeadingMonomial(p[4]),p[7],Abs(p[8])>;
end intrinsic;

intrinsic sort_queue(~queue::SeqEnum[Tup])
{ Sort the queue according to the last component of its entries (assumed
 to all have 9 components)}
    SortKey(~queue, func<p | p[9]>);
    Reverse(~queue);
end intrinsic;

intrinsic remove_from_G(i::RngIntElt, ~G::SeqEnum[Tup],
			~Grem::SetEnum[RngIntElt],
			~queue::SeqEnum[Tup],
			why::MonStgElt)
{ Remove an element from the basis, setting it to 0. Also remove all pairs
involving that element.

Grem is a set keeping track of all such removed elements.
}
    _discard(G[i][1], why : doprint := false);
    G[i][2] := 0;
    Exclude(~Grem,i);
    to_remove := [];
    for kk->cand in queue do
	if cand[1] eq i or cand[2] eq i then
	    _discard(cand[3],"(pair) " cat why
		     : doprint := false);
	    Append(~to_remove,kk);
	end if;
    end for;
    Reverse(~to_remove);
    for kk in to_remove do
	Remove(~queue,kk);
    end for;
end intrinsic;

intrinsic update_syz_basis(~Gz::SeqEnum, s::Sig)
{ Update the basis of syzygies Gz with the signature s. }
    for z in Gz do
	sz,u1,u2 := sigGPair(s,z);
	if u1 ne 0 and u2 ne 0 then
	    Append(~Gz,sz);
	end if;
    end for;
    Append(~Gz,s);
end intrinsic;

intrinsic append_cover_witnesses(~Gc::SeqEnum[Tup],
				 s::Sig,f::RngMPolElt)
{ Add the new element f with signature s to the set of cover witnesses,
 if it is not redundant. Remove redundant elements.

The knowledge of lm(f) is enough. }
    ;
    if exists{j : j in [1..#Gc]
	      | isCoveredBy(s,f,Gc[j][1],Gc[j][2])} then
	;
    else
	to_rem := [j : j in [1..#Gc]
		   | isCoveredBy(Gc[j][1],Gc[j][2],s,f)];
	Reverse(~to_rem);
	for j in to_rem do
	    Remove(~Gc,j);
	end for;
    end if;
end intrinsic;

intrinsic update_cover_witnesses_with_f(~Gc::SeqEnum, ~Gz::SeqEnum,
					s::Sig, f::RngMPolElt)
{ Update the set of cover witnesses when adding a new element f with
signature s. This means adding (s,lm(f)) to Gc, as well as all sigG-pol one
can form with syzygies in the basis Gz }
    for z in Gz do
	sc,u,v := sigGPair(s,z);
	if u ne 0 and v ne 0 then
	    //wit := <sc, LM(u*f)>;
	    append_cover_witnesses(~Gc,sc,LM(u*f));
	end if;
    end for;
    Append(~Gc,<s,f>);
end intrinsic;

intrinsic update_cover_witnesses_with_syz(~Gc::SeqEnum, ~Gz::SeqEnum,
					  z::Sig)
{ Update the set of cover witnesses when adding a new syzygy with
signature z. This means adding to Gc all sigG-pol one
can form with elements of Gc. }
    for w in Gc do
	s,lm := Explode(w);
	sz,u,v := sigGPair(s,z);
	if u ne 0 and v ne 0 then
	    /* wit := <sz, LM(u*lm)>; */
	    append_cover_witnesses(~Gc,sz,LM(u*lm));
	end if;
    end for;
end intrinsic;

intrinsic _can_eliminate_singG(old::Tup, new::Tup)
	  -> BoolElt
{ Returns true if the existence of old in the basis of super reducers 
makes new redundant.

This is the case if new divides old, and if they have the same sig-lead
ratio: then any element which is super reducible by old will also be super
reducible by new.}
    test, ds := IsDivisibleBy(old[1],new[1]);
    return test and LM(ds*new[2]) eq LM(old[2]);
end intrinsic;


intrinsic update_singG(~singG::SeqEnum[Tup],s::Sig,lmf::RngMPolElt)
{ Update the basis of super reducers with a new element

Input:
singG: basis of singular reducers
s: signature of the new element
lmf: leading monomial of the new element

Side effects: singG is updated with the appropriate sigG-combinations, and
redundant elements are discarded
 }
    si,sc,sm := SigExplode(s);
    cands := [<s,LM(lmf)>];
    for sg in singG do
    	sgp,u1,u2 := sigGPair(sg[1],s);
    	if u1 ne 0 and u2 ne 0 and LM(u1*sg[2]) eq LM(u2*lmf) then
    	    Append(~cands,<sgp,LM(u1*sg[2])>);
    	end if;
    end for;

    for sgp in cands do
    	fnd := false;
	to_remove := [];
    	for i->sgpp in singG do
    	    if  _can_eliminate_singG(sgp,sgpp) then
    		fnd := true;
    		break;
	    elif _can_eliminate_singG(sgpp,sgp) then
    		Append(~to_remove,i);
	    end if;
    	end for;
    	if not fnd then
	    Append(~singG,sgp);
    	end if;
	Reverse(~to_remove);
	for i in to_remove do
	    Remove(~singG,i);
	end for;
    end for;
    Sort(~singG, func<a,b | Cmp(a[1],b[1])>);
end intrinsic;

/* Generic algorithm */

intrinsic _GB_algo_KRK_Lichtblau(F::SeqEnum[RngMPolElt]
		       : Signatures := true,
			 Sig_order := "default",
			 interreduce := true,
			 mod_red := true,
			 fail_red0 := false,
			 timeout := Infinity(),
			 experimental := false,
			 KRK := false)
	  -> SeqEnum[RngMPolElt], SeqEnum[Tup],SeqEnum[Sig], SigAlgoCounter
{ Compute a Gröbner basis with signatures using variants of Kandri-Rody and Kapur's or Lichtblau's algorithms.

Input:
- F: sequence of multivariate polynomials over Z

Parameters:
- Signatures (bool, default true): activate signature criteria
- Sig_order (string, default "default"): which signature ordering to use
- interreduce (bool, default true): should we interreduce the basis at
  each step
- mod_red (bool, default true): should we use coefficient reductions in
  the reductions
- fail_red0 (bool, default false): if true, raise an error at the first
  reduction to 0 (intended for debug)
- timeout (number or infinity, default infinity): kill the algorithm
  after that time
- experimental (bool, default false): activate experimental criteria
- KRK (bool, default false): if true, use KRK, else Lichtblau

Output:
- G: Gröbner basis of F
- Gs: Sig-GB of F
- Gz: Sig-basis of syzygies of F
- cnt: counter for pairs, reductions, etc

Notes:
- The value Signatures:=false should be treated as experimental.

- Sig_order can take the following values: "pot" (position over term),
  "top" (term over position), "dpot" (degree over position over term),
  "no_sig" (incremental without signatures, equivalent to
  Signatures:=false) and "default" (currently "pot").

  Currently only "pot" and "no_sig" are implemented (and tested). The
  algorithm is essentially agnostic to the signature order, so adding
  "top" and "dpot" is only a matter of recognizing the corresponding
  strings, for initial tests. The implementation of the F5 criterion
  is not order-agnostic.

- interreduce only takes effect if using an incremental signature
  ordering.

- When called with a finite value of timeout, the actual computation
  time may exceed the timeout value. This is because the timeout is
  only checked when processing a new element, and for large
  computations, that step can take a few seconds.

(Internal function)
  }
    starttime := Realtime();
    lasttime := starttime;
    doprint := false;
    
    if #F eq 0 then return []; end if;

    verb1 := GetVerbose("GroebnerSig") eq 1;
    verb2 := GetVerbose("GroebnerSig") eq 2;
    verb3 := GetVerbose("GroebnerSig") ge 3;
    verb23 := verb2 or verb3;
    verb12 := verb1 or verb2;
    verb123 := verb1 or verb2 or verb3;

    
    if Sig_order eq "default" then
	Sig_order := "pot";
    end if;

    if not Signatures then
	Sig_order := "no_sig";
    end if;
    
    case Sig_order:
    when "pot":
	sig_type := SigPot;
	GGinc := [[] : i in [1..#F]];
	inc := true;
    when "no_sig":
	sig_type := SigNoSig;
	GGinc :=  [[] : i in [1..#F]];
	inc := true; 
    else
	error("Signature order non recognized.");
    end case;

    if KRK then
	update_pairs_fn := update_pairs_KRK;
    else
	update_pairs_fn := update_pairs_Lichtblau;
    end if;
    
    ring := Parent(F[1]);
    
    G := [];
    sigG := [];
    Grem := {};
    ZG := [];
    sigGz := [];
    Gc := []; // Cover witnesses
    singG := []; // List of sig,LT for singular reductions of G-pairs
    singS := []; // List of sig,LM for singular reductions of S-pairs
    from := [];
    types := [];
    lcm_cache := [];
    sing := false;

    sig0 := SigNew(sig_type,0,ring!1);
    cursig := sig0;
    
    pairs := {};
    queue := [];
    emptylist := [];
    maxpairs := 0;

    cnt := New(SigAlgoCounter);
    cnt`algo := Sprintf("%o_%o",
			KRK select "KRK" else "Lichtblau",
			Sig_order);
    cnt`pairs := 0;
    cnt`spol := 0;
    cnt`reds := 0;
    cnt`red0 := 0;
    cnt`syz := 0;
    cnt`f5 := 0;
    cnt`cover := 0;
    cnt`singred := 0;
    cnt`singpair := 0;
    cnt`singpol := 0;
    cnt`coprime := 0;
    cnt`chainF := 0;
    cnt`chainB := 0;
    cnt`chainM := 0;
    cnt`chainG := 0;

    for i->f in F do
	s := SigNew(sig_type,i,ring!1 : d := Degree(f));
	pair := <0,0,s,f,ring!0,ring!0,"N",0>;
	Append(~pair,pair_sorting_key(pair));
	Append(~queue,pair);
    end for;
    SortKey(~queue, func<p | SigKeyMagnitude(p[3])>);
    Reverse(~queue);

    while #queue gt 0 do
	if Realtime() - lasttime ge timeout then
	    print "Exceeded maximal time";
	    return [ring!1], [], [], cnt; // tmp
	end if;
	assert #lcm_cache eq #G;
	/* IndentPop(); */
	next := queue[#queue];
	Remove(~queue,#queue);
	s := next[3];
	if verb3
	   or (verb12 and ((Realtime()-lasttime ge 5) or doprint)) then
	    printf "[%o] (e%o) Queue:%o Basis:%o\n",
		   Realtime(starttime), s`i, #queue, #Grem;
	    doprint := false;
	    lasttime := Realtime();
	end if;
	cmp := cursig cmpeq 0 select -1 else Cmp(cursig,s);
	/* Sanity check: signatures must not decrease */
	assert cmp ne 1; 	

	if cmp eq -1 then
	    lastsig := #G+1;
	end if;
	
	// Simplifications, etc, in incremental case
	if inc and cursig`i lt s`i then
	    if interreduce then
		icur := cursig`i;
		Gnew := [g : g in G | g[1]`i ne 0];
		sigG cat:= Gnew;
		sigGz cat:= ZG;
		Gnosig := [G[i][2] : i in Grem];
		if #Gnosig gt 0 then
		    assert2 IsGroebner(Gnosig);
		end if;
		Gnosig := ReduceGroebnerBasis_Z(Gnosig);
		G := [<sig0,g> : g in Gnosig];
		lcm_cache := [[<Lcm(LM(Gnosig[i]),LM(Gnosig[j])),0>
			       : i in [1..j]]
			      : j in [1..#Gnosig]];
		from := [<0,0> : g in G];
		type := ["O" : g in G];
		singG := [];
		singS := [];
		Grem := {1..#G};
	    end if;
	    // For F5
	    ZG := [SigNew(sig_type,s`i,LT(G[i][2])) : i in Grem];
	    Gc := [];
	end if;
	/* IndentPush(); */
	
	cursig := SigCopy(s);
	
	if next[1] eq 0 then
	    i,j,s,f,_,_,type,_ := Explode(next);
	else
	    cnt`pairs +:= 1;
	    i,j,s,tdeg,dt1,dt2,type,_ := Explode(next);

	    vprintf GroebnerSig,3:
		"  Next signature: %o - %o(%o,%o)\n", _sigstr(s), type, i,j;

	    // Sig lc reduction
	    for sz in ZG do
		s mod:= sz;
		if s`c eq 0 then
		    break;
		end if;
	    end for;

	    // Syzygy + F5 criterion
	    if /* exists{sz : sz in ZG */
		/* 	      | IsDivisibleBy(s,sz)}  */
		s`c eq 0 then
		cnt`syz +:= 1;
		_discard(cursig,"syzygy criterion");
		continue;
	    end if;

	    /* // Cover criterion? */
	    /* if type eq "S" then */
	    if experimental or (KRK and type eq "S") then
		who := 0;
		coverCriterion(s,tdeg,~Gc,~who);
		if who ne 0 then
		    cnt`cover +:= 1;
		    _discard(s,Sprintf("cover criterion (by %o)",who) : doprint := false);
		    continue;
		end if;
	    end if;
	    /* end if; */

	    
	    // Singular criterion
	    // No singular criterion with Lichtblau? (some reductions are
	    // handled like S-pols)
	    if Signatures and (KRK or experimental) then
		sbis := ring!(-1)*s;
		if /* type eq "S" */
		    /* and */ exists{i : i in Grem | SigEqSign(G[i][1],s)} then
		    cnt`singpol +:= 1;
		    _discard(s,"singular criterion");
		    continue;
		end if;
	    end if;

	    // Else we compute the S-polynomial
	    cnt`spol +:= 1;
	    f := dt1*G[i][2] + dt2*G[j][2];

	    /* Sanity check: the LT of a S-pol or a G-pol cannot be larger
	       than the lcm of the LTs */
	    lmf := f ne 0 select LM(f) else ring!0;
	    assert lmf le tdeg;

	    // Normalization
	    /* if s`c lt 0 then */
	    /* 	s *:= ring!(-1); */
	    /* 	f *:= -1; */
	    /* end if; */
	end if;

	if verb3 then
	    printf "  Next pol: %o (sig=%o) - %o(%o,%o)\n",
		   _polstr(f),_sigstr(s), type,i,j;
	end if;
	
	if f eq 0 then
	    _discard(s,"polynomial is zero");
	    continue;
	end if;

	// Now we reduce
	sing := false;
	cnt`reds +:= 1;

	ff := f;
	if not Signatures then
	    f := NormalForm(f,[g[2] : g in G]);
	else
	    StrongReduceHead(~s,~f,~G,~Grem :
			     mod_red := mod_red, print_red:=false);
	end if;

	if f eq 0 and not sing then
	    /* if sig_type eq SigPot then */
	    vprintf GroebnerSig, 1:
		"  !!! Reduction to 0 !!! %o\n", s;
	    if fail_red0 then
		error "Reduction to zero, stopping as requested";
	    end if;
	    cnt`red0 +:= 1;
	    if Signatures then
		update_syz_basis(~ZG,s);
		update_cover_witnesses_with_syz(~Gc,~ZG,s);
	    end if;
	    continue;
	end if;

	// super criterion
	if Signatures then
	    who := 0;
	    if type eq "G" then
		IsSuperReducibleByList(s,LT(f),~singG,~who
					   : strict := true);
	    elif type eq "S" and not KRK then
		IsSuperReducibleByList(s,LM(f),~singS,~who
					   : strict := false);
	    end if;
	    if who ne 0 then
		/* vv := GetVerbose("GroebnerSig"); */
		/* SetVerbose("GroebnerSig",3); */
		cnt`singred +:= 1;
		_discard(s,Sprintf("(%o) super reducible by %o (lt=%o)",
				   type,who,_terstr(LT(f))));
		/* SetVerbose("GroebnerSig",vv); */
		continue;
	    end if;

	    // Backward super criterion
	    to_remove := [];
	    newSing := [<s,LM(f)>];
	    for ii in [lastsig..#G] do
		g := G[ii];
		sg := g[1];
		gg := g[2];
		if ii in Grem then
		    test, dc := IsDivisibleBy(sg`c,s`c);
		    if test and //(LM(gg) gt LM(f) or
		       ((types[ii] ne "G" and LM(gg) eq LM(f))
			or (types[ii] eq "G" and LT(gg) eq dc*LT(f)))//)
			then
			/* sing := false; */
			/* IsSuperReducibleByList(g[1],LM(g[2]),~newSing, ~sing,~who); */
			/* if sing then */
			remove_from_G(ii,~G,~Grem,~queue,
				      "super reducible by new elt");
		    end if;
		end if;
	    end for;

	    /* to_remove := Setseq(Seqset(to_remove)); */
	    /* Sort(~to_remove); */
	    /* Reverse(~to_remove); */
	    /* for ii in to_remove do */
	    /* 	Remove(~queue,ii); */
	    /* end for; */
	    
	    // Tail reduction
	    if Signatures and ff ne f then
		StrongReduceTail(~s,~f,~G, ~Grem : mod_red := mod_red, print_red:=false);
	    end if;
	end if;
	
	if verb23 then
	    printf "  After reduction -> [#%o]: %o\n",#G+1, _polstr(f);
	end if;

	if LC(f) lt 0 then
	    f := -f;
	    s`c := -s`c;
	end if;
	
	lmf := LM(f);
	update_pairs_fn(~queue, ~lcm_cache, <s,f>, ~G, ~Grem, ~cnt);
	maxpairs := Max(maxpairs,#queue);
	Append(~G,<s,f>);
	Include(~Grem,#G);
	Append(~singG,<s,LT(f)>);
	update_cover_witnesses_with_f(~Gc,~ZG,s,f);
	if Signatures and not KRK then
	    Append(~singS,<s,LM(f)>);
	    update_singG(~singS,s,LM(f));
	end if;
	Append(~from,<i,j>);
	Append(~types,type);

	/* Forward criteria */
	to_remove := [];
	for kk->p in queue do
	    _,_,sp,mp,_,_,typep,cp := Explode(p);
	    /* if sp eq s and mp eq tdeg and cp eq c then */
	    /* 	_discard(sp,"duplicate element"); */
	    /* end if; */
	    if Signatures and (experimental or (KRK and type eq "S"))
	       and isCoveredBy(sp,mp,s,lmf) then
		Append(~to_remove,kk);
		cnt`cover +:= 1;
		_discard(sp,"cover criterion (pair)");
	    end if;
	end for;
	Reverse(~to_remove);
	for kk in to_remove do
	    Remove(~queue,kk);
	end for;
	
	doprint := true;
	/* IndentPop(2); */
	
    end while;

    cnt`tottime := Realtime(starttime);
    cnt`maxpairs := maxpairs;
    cnt`lastsig := cursig;
    
    Gnosig := [G[i][2] : i in Grem];
    assert2 IsGroebner(Gnosig);
    if interreduce then
	icur := cursig`i;
	Gnew := [g : g in G | g[1]`i eq icur];
	sigG cat:= Gnew;
	sigGz cat:= ZG;
	Gnosig := ReduceGroebnerBasis_Z(Gnosig);
	return Gnosig, sigG, sigGz, cnt;
    else
	return Gnosig, G, ZG, cnt;
    end if;
end intrinsic;

/* Wrappers for Kandri-Rody and Kapur's algorithms */

intrinsic GB_KandriRodyKapur(F::SeqEnum
			     : Signatures := true,
			       Sig_order := "default",
			       interreduce := true,
			       mod_red := true,
			       fail_red0 := false,
			       timeout := Infinity(),
			       experimental := false)
	  -> SeqEnum[RngMPolElt], SeqEnum[Tup],SeqEnum[Sig],
	     SigAlgoCounter
{ See the docstring of _GB_algo_KRK_Lichtblau with KRK set to true }
    G,Gs,Gz,cnt := _GB_algo_KRK_Lichtblau(F 
					  : Signatures:=Signatures,
					    Sig_order := Sig_order,
					    interreduce := interreduce,
					    mod_red := mod_red,
					    fail_red0 := fail_red0,
					    timeout := timeout,
					    experimental := experimental,
					    KRK := true);
    return G,Gs,Gz,cnt;
end intrinsic;


intrinsic GB_Lichtblau(F::SeqEnum
		       : Signatures := true,
			 Sig_order := "default",
			 interreduce := true,
			 mod_red := true,
			 fail_red0 := false,
			 timeout := Infinity(),
			 experimental := false)
	  -> SeqEnum[RngMPolElt], SeqEnum[Tup],SeqEnum[Sig],
	     SigAlgoCounter
{ See the docstring of _GB_algo_KRK_Lichtblau with KRK set to false }
    G,Gs,Gz,cnt := _GB_algo_KRK_Lichtblau(F 
					  : Signatures:=Signatures,
					    Sig_order := Sig_order,
					    interreduce := interreduce,
					    mod_red := mod_red,
					    fail_red0 := fail_red0,
					    timeout := timeout,
					    experimental := experimental);
    return G,Gs,Gz,cnt;
end intrinsic;

/* Moeller's algorithm */

intrinsic GB_Moeller(F::SeqEnum[RngMPolElt]
		     : Sig_order := "default",
		       timeout := 0,
		       fail_red0 := false,
		       experimental := false,
		       interreduce := true)
	  -> SeqEnum[RngMPolElt], SeqEnum[Tup], SeqEnum[Tup],
	     SeqEnum[Sig], SigAlgoCounter
{ Compute a Gröbner basis with signatures using Moeller's algorithm

Input:
- F: sequence of multivariate polynomials over Z

Parameters:
- Sig_order (string, default "default"): which signature ordering to use
- interreduce (bool, default true): should we interreduce the basis at
  each step
- fail_red0 (bool, default false): if true, raise an error at the first reduction to 0 (intended for debug)
- experimental (bool, default false): enable experimental criteria
- timeout (number or infinity, default infinity): kill the algorithm after
  that time

Output:
- G: strong Gröbner basis of F
- Gw: weak Sig-GB of F
- Gs: strong Sig-GB of F
- Gz: Sig-basis of syzygies of F
- cnt: counter for pairs, reductions, etc

Notes:
- Sig_order can take the following values: "pot" (position over term),
  "top" (term over position), "dpot" (degree over position over term),
  "no_sig" (incremental without signatures, equivalent to
  Signatures:=false) and "default" (currently "pot").

  Currently the orders "pot", "top" and "dpot" are implemented. ToP
  and DPoT should be treated as experimental.

- interreduce only takes effect if using an incremental signature ordering.

- if interreduce is true, the algorithm only return correct signatures
  with respect to the last component, all smaller signatures are set
  to e0.

- When called with a finite value of timeout, the actual computation
  time may exceed the timeout value. This is because the timeout is
  only checked when processing a new element, and for large
  computations, that step can take a few seconds.
}
    if #F eq 0 then return [],[],[]; end if;

    /* Assertions level 2:
    - sequence is not regular if reduction to 0
    - intermediate GB is Groebner before inter-reducing
    
    Print level: GroebnerSig
     1: basic progress update
     2:	each time the signature increases, info on pairs taking more than 1s,
     info on added pairs
     3: info on all pairs
    
   */

    verb1 := GetVerbose("GroebnerSig") eq 1;
    verb2 := GetVerbose("GroebnerSig") eq 2;
    verb3 := GetVerbose("GroebnerSig") ge 3;
    verb23 := verb2 or verb3;
    verb12 := verb1 or verb2;
    verb123 := verb1 or verb2 or verb3;

    /* if GetAssertions() ge 2 then */
    /* 	regseq := [IsRegularSequence(F[1..i]) : i in [1..#F]]; */
    /* end if; */
    regseq := [false : i in [1..#F]];

    starttime := Realtime();
    lasttime := 0;
    cursig := 0;
    start := true;
    cntsteps := 0;
    maxpairs := 0;

    if Sig_order eq "default" then
	if forall{f : f in F | IsHomogeneous(f)} then
	    Sig_order := "dpot";
	else
	    Sig_order := "pot";
	end if;
    end if;
    
    case Sig_order:
    when "pot":
	sig_type := SigPot;
	GGinc := [[] : i in [1..#F]];
	inc := true;
    when "top":
	sig_type := SigTop;
	inc := false;
    when "dpot":
	sig_type := SigDPot;
	GGinc := [[] : i in [1..#F]];
	inc := true;
    else
	error("Signature order non recognized.");
    end case;

    cnt := New(SigAlgoCounter);
    cnt`algo := Sprintf("Moeller_%o",Sig_order);
    cnt`pairs := 0;
    cnt`spol := 0;
    cnt`reds := 0;
    cnt`red0 := 0;
    cnt`syz := 0;
    cnt`f5 := 0;
    cnt`singred := 0;
    cnt`singpair := 0;
    cnt`singpol := 0;
    cnt`cover := 0;
    cnt`coprime := 0;
    cnt`chainF := 0;
    cnt`chainB := 0;
    cnt`chainM := 0;
    
    // Initialization
    ring := Parent(F[1]);
    ltinput := [LT(f) : f in F];
    SGw := [];
    SGs := [];
    ZG := [];
    Gsrem := {};
    Gwrem := {};
    singG := [];
    seen_sigcoefs := [];
    lcm_cache := [];
    queue := [];
    doprint := false;
    from := [];

    emptylist := [];
    
    for i->f in F do
	s := SigNew(sig_type,i,ring!1 : d := Degree(f));
	pair := <0,0,s,f,ring!0,ring!0>;
	Append(~pair,pair_sorting_key_Moeller(pair));
	Append(~queue,pair);
    end for;
    SortKey(~queue, func<p | SigKeyMagnitude(p[3])>);
    Reverse(~queue);

    pr_first := true;
    pr_pair := <0,0>;
    pr_sig := 0;
    pr_res := "";
    pr_start := 0;

    // Main loop
    /* try */
    while #queue gt 0 do
	if timeout gt 0 and Realtime(starttime) gt timeout then
	    error("Timeout");
	end if;

	// Preparing for profiling the pair
	if verb123 then
	    if pr_first then
	    	pr_first := false;
	    else
	    	pr_time := Realtime(pr_start);
		if verb3 or pr_time ge 1
		   or (verb2 and doprint)
		    then
	    	    printf "%o %o \t-> %o \t[%o]\n",
	    		   pr_pair, pr_sig, pr_res, pr_time;
		end if;
	    end if;
	    pr_start := Realtime();
	end if;
	
	if (verb12 and Realtime() - lasttime gt 5)
	   or (verb23 and doprint) then
	    printf "[%o] Queue:%o, Weak:%o, Strong:%o, Syz:%o, Sing:%o",
		   Realtime(starttime),
		   #queue, #SGw, #SGs, #ZG, #singG;
	    if start then
		start := false;
		printf ", starting\n";
	    else
		printf ", done %o\n", cntsteps; 
		cntsteps := 0;
	    end if;
	    lasttime := Realtime();
	    doprint := false;
	end if;

	cntsteps +:= 1;
	if #queue gt maxpairs then
	    maxpairs := #queue;
	end if;

	only_G := false;
	next := queue[#queue];
	Remove(~queue,#queue);

	s := next[3];
	pr_sig := s;
	cmp := cursig cmpeq 0 select -1
	       else Cmp(cursig,s);
	assert cmp ne 1;
	if cmp eq -1 then
	    // Update the strong GB
	    if cursig cmpne 0 then
		update_SGB_batch(~SGs,~SGw,~Gsrem,cursig);
		// Restart the timer
		pr_start := Realtime();
	    end if;
	    
	    // Prepare for F5 for incremental or half-incremental orders
	    if inc and cursig cmpne 0 then
		previ := cursig`i;
		newi := s`i;
		if newi ne previ then
		    if interreduce then
			GG := [sg[2] : sg in SGs
			       | sg[1]`i le previ];
			assert2 IsGroebner(GG);
			G := ReduceGroebnerBasis_Z(GG);
			// assert2 IsGroebner(G);
			
			if Sig_order eq "pot" then
			    vprintf GroebnerSig, 1:
				"New index: %o\n", newi;
			    SGw := [<SigNew(sig_type,0,ring!1 : d := Degree(g)),g> : g in G];
			    SGs := SGw;
			    Gsrem := {i : i in [1..#SGs]};
			    Gwrem := {i : i in [1..#SGw]};
			    singG := [];
			    from := [<0,0> : g in SGs];
			    lcm_cache := [[Lcm(LT(G[i]),LT(G[j])) : i in [1..j]]
					  : j in [1..#G]];
			    curipos := #SGw;
			end if;
			if Sig_order eq "dpot" then
			    GGinc[previ] := [LT(g) : g in G];
			    vprintf GroebnerSig, 1:
				"New deg,idx: %o,%o\n", s`d,newi;
			end if;
		    end if;
		    if Sig_order eq "pot" then
			ZG := [SigNew(sig_type,newi,LT(g[2])) : g in SGs];
		    end if;
		end if;
	    end if;

	    seen_sigcoefs := [];
	    cursig := SigCopy(s);
	    cursig`c := 1;
	    curipos := #SGw;

	    vprintf GroebnerSig, 2:
	        "New signature: %o\n", cursig;
	    
	end if;

	/* The only difference between what we do for a new polynomial
	from F and a S-pair is in obtaining the polynomial and in
	whether we can apply criteria. */
	if next[1] eq 0 then
	    // New polynomial
	    i,j,_,f := Explode(next);
	else
	    // S-pair
	    cnt`pairs +:= 1;
	    i,j,_,tdeg,dt1,dt2 := Explode(next);

	    pr_pair := <i,j>;
	    
	    assert i lt j;

	    // F5 criterion
	    // For pot it goes in the syzygy criterion
	    // Actually for dpot it should too
	    /* if Sig_order eq "dpot" then */
	    /* 	test := false; */
	    /* 	Sig_f5_idx(~test,s,~GGinc,~sig_tuple); */
	    /* 	if test then */
	    /* 	    cnt`f5 +:= 1; */
	    /* 	    /\* vprintf GroebnerSig, 2: "-> Eliminated by F5 criterion\n"; *\/ */
	    /* 	    pr_res := "Ignored: F5"; */
	    /* 	    continue; */
	    /* 	end if; */
	    /* end if; */

	    // Syzygy criterion
	    // FIXME
	    for sz in ZG do
		s mod:= sz;
	    	if s`c eq 0 then
		    // Dealt with just below
		    // The reason is that we want to break out of this
		    // loop but continue the while
	    	    break;
	    	end if;
	    end for;
	    
	    if s`c eq 0 then
		// With quorem reductions we can't really make a
		// difference between syz and f5
	    	cnt`syz +:= 1;
		/* vprintf GroebnerSig, 2: "-> Eliminated by syzygy criterion\n"; */
		pr_res := "Ignored: syz";
		if GetAssertions() ge 3 /* and Degree(s[2]) ge 11  */then
		    f := dt1*SGw[i][2] + dt2*SGw[j][2];
		    StrongReduceHead(~s,~f,~SGs,~Gsrem);
		    assert3 f eq 0;
		end if;
		
	    	continue;
	    end if;
	    
	    // /* Cover criterion */
	    if experimental then
		who := 0;
		coverCriterion(s,tdeg,~SGw,~Gwrem,~who);
		if who ne 0 then
		    cnt`cover +:= 1;
		    pr_res := Sprintf("Ignored: cover (%o)",who);
		    //error "debug here";
		    /* only_G := true; */
		end if;
	    end if;
	    
	    // /* Singular criterion */
	    cc := Abs(s`c);
	    if cc in seen_sigcoefs then
		cnt`singpol +:= 1;
		pr_res := "Ignored: singular";
		continue;
	    else
		Append(~seen_sigcoefs,cc);
	    end if;

	    // If we reach this point we compute the S-polynomial
	    cnt`spol +:= 1;
	    f := dt1*SGw[i][2] + dt2*SGw[j][2];
	    ff := f;
	end if;

	if f eq 0 then
	    continue;
	end if;

	/* Now we reduce the polynomial */
	sing := false;

	cnt`reds +:= 1;
	StrongReduceHead(~s,~f,~SGs,~Gsrem : mod_red := true);
	
	if f eq 0 and not sing then
	    if sig_type eq SigPot then
		vprintf GroebnerSig, 1: "!!! Reduction to 0 !!! %o\n", s;
		/* error("debug here"); */
		assert2 not regseq[newi]; // Does not cover top or dpot
		if fail_red0 then
		    error("Reduction to 0");
		end if;
	    end if;
	    cnt`red0 +:= 1;
	    Append(~ZG,s);
	    pr_res := "ZERO";
	    continue;
	end if;

	// super criterion
	/* TODO: don't check in case it was a new polynomial? */
	sing := false;
	who := 0;
	IsSuperReducibleByList(s,LM(f),~singG,~who);

	if who ne 0 then
	    cnt`singred +:= 1;
	    pr_res := "1-sing red";
	    continue;
	end if;

	
	/* We know the LT cannot be reduced further */
	bef := #Terms(f);
	StrongReduceTail(~s,~f,~SGs,~Gsrem : mod_red := true);
	aft := #Terms(f);

	doprint := verb2;
	pr_res := Sprintf("Added #%o with lt=%o (%o->%o)", #SGw+1, _terstr(LT(f)), bef, aft);

	lmf := LM(f);
	update_singG(~singG,s,lmf);
	update_pairs_Moeller(~queue,~lcm_cache,<s,f>,~SGw,~cnt);
	Append(~from,<i,j>);
	Append(~SGw,<s,f>);
	Include(~Gwrem,#SGw);
	
    end while;

    // Last print
    if verb3 or (verb2 and pr_time ge 1) then
	printf "%o \t-> %o \t[%o]\n",
	       pr_sig, pr_res, pr_time;
    end if;

    G := [sg[2] : sg in SGs];
    if interreduce then
	assert2 IsGroebner(G);
	G := ReduceGroebnerBasis_Z(G);
    end if;
    if GetVerbose("GroebnerSig") ge 1 then
	cnt`tottime := Realtime(starttime);
	cnt`maxpairs := maxpairs;
	cnt`lastsig := cursig;
    end if;
    return G,SGw,SGs,ZG,cnt;
end intrinsic;





