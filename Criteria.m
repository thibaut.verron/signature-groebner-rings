/* Various criteria for the algorithms */

/* Generic functions */

LT := LeadingTerm;
LC := LeadingCoefficient;
LM := LeadingMonomial;

intrinsic MyIsDivisibleBy(a::RngIntElt, b::RngIntElt) -> Bool
{ Divisibility test for positive integers. Skips the divisibility test if the sizes don't match. }
    return (a) ge (b) and IsDivisibleBy(a,b);
end intrinsic;

/* Chain criterion for S-polynomials */

intrinsic chain_criterion_B(lcms::Tup, sij::Sig,
			    tr::RngMPolElt, sr::Sig) -> BoolElt
{ Returns true if Gebauer-Möller's B criterion holds, that is, the pair
(i,j) allows to eliminate the new element r.

Input:
lcms: <t_ij,t_ir,t_rj> where t_ab = lcm(t_a,t_b)
s_ij: signature of the pair (i,j)
t_r: leading term of the new element
s_r: signature of the new element

In Lichtblau's algorithms, the terms are monomials. }
    tij, tir, tjr := Explode(lcms);
    test, dt := IsDivisibleBy(tij, tr);
    if not test then
	return false;
    else
	return tir ne tij and tjr ne tij
	       and SigCmpProd(sr,dt,sij) lt 0;
    end if;
end intrinsic;

intrinsic chain_criterion_M(lcms::Tup, sir::Sig,
			    tj::RngMPolElt, sj::Sig)
	  -> BoolElt
{ Returns true if Gebauer-Möller's M criterion holds, that is, the pair
(i,j) allows to eliminate the new element r.

Input:
lcms: <t_ij,t_ir,t_rj> where t_ab = lcm(t_a,t_b)
s_ij: signature of the pair (i,j)
t_r: leading term of the new element
s_r: signature of the new element

In Lichtblau's algorithms, the terms are monomials. }
    tij, tir, tjr := Explode(lcms);
    return tjr ne tir
	   and IsDivisibleBy(tir,tjr)
	   and SigCmpProd(sj,tir,sir,tj) lt 0;
end intrinsic;

intrinsic chain_criterion_F(lcms::Tup, sir::Sig,
			    tj::RngMPolElt, sj::Sig)
	  -> BoolElt
{ Returns true if Gebauer-Möller's M criterion holds, that is, the pair
(i,j) allows to eliminate the new element r.

Input:
lcms: <t_ij,t_ir,t_rj> where t_ab = lcm(t_a,t_b)
s_ij: signature of the pair (i,j)
t_r: leading term of the new element
s_r: signature of the new element

In Lichtblau's algorithms, the terms are monomials. }
    tij, tir, tjr := Explode(lcms);
    return tjr eq tir
	   and SigCmpProd(sj,tir,sir,tj) lt 0;
end intrinsic;

/* Chain criterion for G-polynomials */

intrinsic chain_criterion_G(tij::RngMPolElt, tr::RngMPolElt,
			    sij::Sig, sr::Sig
			    : strict := false)
	  -> BoolElt
{ Chain criterion for G-polynomials: returns true if the G-polynomial
tij allows to discard tr

If strict is false, also allow cases where the signatures are an exact
match if the terms also are an exact match. }
    test, dt := IsDivisibleBy(tij,tr);
    return test
	   and (SigCmpProd(sr,dt,sij) lt 0
		or (not strict and dt*sr eq sij));
end intrinsic;

intrinsic chain_criterion_G(tij::RngElt, tr::RngElt,
			    sij::Sig, sr::Sig
			    : strict := false)
	  -> BoolElt
{ See the docstring of chain_criterion_G.

Wrapper for the case where one of the terms has degree 0.}
    P := PolRing(sr);
    return chain_criterion_G(P!tij,P!tr,sij,sr : strict := strict);
end intrinsic;


/* Divisibility tests for the chain criterion in Lichtblau */

intrinsic chain_criterion_lc(c1::RngIntElt, c2::RngIntElt, c3::RngIntElt)
	  -> BoolElt
{ Test whether c1 | c3 | c2 or c2 | c3 | c1 or c3 | c1 | c2
  or c3 | c2 | c1	}
    t12 := IsDivisibleBy(c2,c1);
    t13 := IsDivisibleBy(c3,c1);
    t21 := not t12 and IsDivisibleBy(c1,c2);
    t23 := IsDivisibleBy(c3,c2);
    t31 := not t13 and IsDivisibleBy(c1,c3);
    t32 := not t23 and IsDivisibleBy(c2,c3);
    return (t13 and t32) or (t31 and t12)
	   or (t23 and t31) or (t32 and t21);
end intrinsic;

intrinsic chain_criterion_lc_with_13(test::RngIntElt, c1::RngIntElt, c2::RngIntElt, c3::RngIntElt) -> BoolElt
{ Same as chain_criterion_lc, but given that test is -1 iff c1 divides c3, and test is 1 iff c3 divides c1.

Assumes that the coefficients are all positive. }
    case test:
    when 0:
	return false;
    when -1:
	assert2 IsDivisibleBy(c3,c1);
	return MyIsDivisibleBy(c2,c3);
    when 1:
	assert2 IsDivisibleBy(c1,c3);
	return MyIsDivisibleBy(c2,c1)
	       or MyIsDivisibleBy(c3,c2)
	       or (MyIsDivisibleBy(c2,c3) and MyIsDivisibleBy(c1,c2));
    end case;
end intrinsic;

intrinsic chain_criterion_lc_with_12(test::RngIntElt, c1::RngIntElt,
				     c2::RngIntElt, c3::RngIntElt) -> BoolElt
{ Same as chain_criterion_lc, but given that test is -1 iff c1 divides c2, and test is 1 iff c2 divides c1.

Assumes that the coefficients are all positive. }
    if test eq 0 then
	return false;
    end if;
    /* if test eq 1 then */
    /* 	c1,c2 := Explode(<c2,c1>); */
    /* end if; */
    return MyIsDivisibleBy(c1,c3)
	   or (MyIsDivisibleBy(c3,c1) and MyIsDivisibleBy(c2,c3));
end intrinsic;

/* Cover criterion */

intrinsic isCoveredBy(sf::Sig,f::RngMPolElt,
			sg::Sig,g::RngMPolElt)
	  -> Bool
{ Return true iff (sf,f) is covered by (sg,g)

Assumes that f and g are monomials }    
    test,dt := IsDivisibleBy(sf,sg);
    return test and LM(dt*g) lt LM(f);
end intrinsic;

intrinsic coverCriterion(s::Sig, f::RngMPolElt, ~G::SeqEnum[Tup],
			  ~who::RngIntElt)
{ Test for the cover criterion. 

Input:
s: signature of the element to test
f: polynomial to test
G: signature Gröbner basis (or set of cover witnesses)
who: integer, placeholder

Output: none

Side effects: if (s,f) is covered by G[i], who is set to i. Else who is 0.
 }
    ;
    test := exists(j){j : j in [1..#G]
		      | /* G[j][2] ne 0 and  */isCoveredBy(s,f,G[j][1],LM(G[j][2]))};
    who := test select j else 0;
end intrinsic;

/* Super reducible */

intrinsic IsSuperReducible(sf::Sig,ltf::RngMPolElt,
			       sg::Sig,ltg::RngMPolElt
			       : strict := false)
	  -> BoolElt
{ Test whether (sf,ltf) is super reducible by (sg,ltg)

Input:
sf: signature of the new element
ltf: leading term of the new element
sg: signature of the potential reducer
ltg: leading term of the potential reducer

Parameters: 
strict: require an exact match for the leading terms (including
coefficients)

Output: true iff (sf,ltf) is super reducible by (sg,ltg)
}
    test, ds := IsDivisibleBy(sf,sg);
    return test and LM(ds*ltg) eq LM(ltf)
	   and (not strict or LC(ds*ltg) eq LC(ltf));
end intrinsic;

intrinsic IsSuperReducibleByList(s::Sig,ltf::RngMPolElt,
				     ~singG::SeqEnum[Tup],
				     ~who::RngIntElt
				     :strict:=false)
{ Test whether (s,ltf) is super reducible by a list

Input:
s: signature of the new element
ltf: leading term of the new element
singG: signature Gröbner basis, or basis of singular reducers
who: placeholder

Parameters:
strict: require an exact match for the leading terms (including
coefficients)

Output: none

Side effects: if (s,ltf) is super reducible by singG[i], who is set to i. 
Otherwise who is set to 0.
 }
    //TODO: for checking the same signature many times in the middle of
    //reductions we can be a lot faster by precomputing a list of
    //candidates, or even a list of LT to compare for equality
    ; // for syntax highlighting
    res := exists(j){i : i in [1..#singG]
	   	     | IsSuperReducible(
			       s,ltf, singG[i][1], LT(singG[i][2])
			       : strict := strict)};
    if res then
	who := j;
    end if;
end intrinsic;
