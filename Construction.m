/* Constructions for the algorithms */

LT := LeadingTerm;
LC := LeadingCoefficient;
LM := LeadingMonomial;

/* S-pairs */



intrinsic SPair(s1::Sig, f1::RngMPolElt,
		s2::Sig, f2::RngMPolElt)
	  -> Sig, RngMPolElt, RngMPolElt, RngMPolElt, RngIntElt
{ Compute data allowing to construct a S-polynomial

Input:
s1, f1: signature and polynomial of f1
s2, f2: signature and polynomial of f2

Output:
sig: signature of the S-polynomial (0 if the S-polynomial if not regular
     or is caught by the coprime criterion)
tdeg: lcm of lt(f1), lt(f2)
dt1, dt2: SPol(f1,f2) = dt1*f1 + dt2*f2
res: 0 if coprime, 1 if regular, 2 if singular 

Note: the function does not need the full polynomials to work, their
leading terms are sufficient.  }
    /* s1,f1 := Explode(sf1); */
    /* s2,f2 := Explode(sf2); */
    t1 := LT(f1);
    t2 := LT(f2);
    mt12 := Lcm(t1,t2);
    
    
    dt1 := mt12 div t1;
    
    // /* Following CLO, should we apply the coprime criterion when
    // processing the pairs, so that they are not used to eliminate chain
    // pairs prematurely? */
    if dt1 eq t2 or dt1 eq -t2 then
	return SigNull(Type(s1)),mt12,0,0,0;
    end if;
    
    dt2 := -mt12 div t2;
    
    ss1 := dt1*s1;
    ss2 := dt2*s2;
    case Cmp(ss1,ss2):
    when -1:
	return ss2,mt12,dt1,dt2,1;
    when 1:
	return ss1,mt12,dt1,dt2,1;
    else
	/* Non-regular S-pair */
	return SigNull(Type(s1)),mt12,0,0,2;
    end case;
    // Last coord is 0 if coprime, 1 if regular, 2 if singular
    // FIXME: Improve that interface
end intrinsic;

intrinsic GPair(~sf1::Tup, ~sf2::Tup, ~res::Tup : true_sig := false)
{ Compute a G-polynomial

Input:
sf1: pair (signature, polynomial) for f1
sf2: pair (signature, polynomial) for f2
res: placeholder

Parameter: 
true_sig: if true, the signature of the result is computed with
coefficient, and the Bézout coefficients are chosen to ensure that it is
not zero

Output: none

Side effects: res is updated to be the tuple <s,f> = GPol(f1,f2)
}
    // FIXME: Harmonize the interface for all the "pair" functions
    s1,f1 := Explode(sf1);
    s2,f2 := Explode(sf2);
    m1 := LM(f1);
    m2 := LM(f2);

    m12 := Lcm(m1,m2);

    dm1 := m12 div m1;
    dm2 := m12 div m2;
    ss1 := dm1 * s1;
    ss2 := dm2 * s2;
    if ss1 lt ss2 then
	s12 := ss2;
    else
	s12 := ss1;
    end if;

    _,dc1,dc2 := ExtendedGreatestCommonDivisor(LC(f1),LC(f2));

    if true_sig then
	if dc1 eq 0 or dc2 eq 0 then
	    s12 := SigNew(Type(ss1),ss1`i,dc1*s1`c + dc2*s2`c, ss1`m);
	else
	    ss1 := (dc1*dm1)*s1;
	    ss2 := (dc2*dm2)*s2;
	    if ss1 gt ss2 then
		s12 := ss1;
	    elif ss1 lt ss2 then
		s12 := ss2;
	    else
		if ss1`c * ss1`m eq -ss2`c * ss2`m then
		    //error "debug here";
		    dc1 +:= LeadingCoefficient(f2);
		    dc2 -:= LeadingCoefficient(f1);
		end if;
		s12 := SigNew(Type(ss1),ss1`i,dc1*s1`c + dc2*s2`c, ss1`m);
	    end if;
	end if;
    end if;
    res := <s12,dc1*dm1*f1 + dc2*dm2*f2>;
end intrinsic;


/* G-pairs */

intrinsic sig_lm_gpair(s1::Sig,m1::RngMPolElt,s2::Sig,m2::RngMPolElt)
	  -> Tup
{ Compute the signature and the LM of a G-polynomial }
    m12 := Lcm(m1,m2);
    dm1 := m12 div m1;
    dm2 := m12 div m2;
    sm1 := dm1*s1;
    sm2 := dm2*s2;
    if sm1 lt sm2 then
	return <sm2,m12>;
    else
	return <sm1,m12>;
    end if;
end intrinsic;

/* SG-pairs (Lichtblau and Kandri-Rody/Kapur) */

intrinsic SGPair(s1::Sig, f1::RngMPolElt,
		 s2::Sig, f2::RngMPolElt
		 : force := "N")
	  -> Sig, RngMPolElt, RngMPolElt, RngMPolElt, MonStgElt, RngIntElt
{ Return the construction of the SG-polynomial of f1 (sig s1) and f2
(sig s2)

Optional argument:
force (string, default "N"): if "S" or "G", force the output to be of
this type. 

Output:
s, m, dt1, dt2, type, c
such that, if g is the S or G-pol of f1 and f2:
- s is the signature of g
- m is the lcm of the LM of f1 and f2
- g = dt1*f1 + dt2*f2
- type is "S" if g is a S-pol, "G" if g is a G-pol
- c is the lcm of the LC of f1 and f2 (if S-pol) or their gcd (if G-pol)

In Lichtblau's algorithm (if forced is "N"), s is 0 if the operation is
singular and the type is S. 

In Kandri-Rody and Kapur's algorithm (if forced is "S" or "G"), s is 0 if
the type is S and the operation is not regular.  If the type is G and the
G-polynomial is trivial (equal to f1 or f2), s is also 0.

In both cases, if the type is G, the cofactors dt1 and dt2 are chosen to
ensure that the operation is not singular.

 }
    m1 := LM(f1);
    m2 := LM(f2);

    m12 := Lcm(m1,m2);
    dm1 := m12 div m1;
    dm2 := m12 div m2;

    c1 := LC(f1);
    c2 := LC(f2);
    
    if force eq "S" then
	forced := true;
	type := "S";
	who := 0;
    elif force eq "G" then
	forced := true;
	type := "G";
    elif IsDivisibleBy(c2,c1) then
	forced := false;
	type := "S";
	who := 2;
    elif IsDivisibleBy(c1, c2) then
	forced := false;
	type := "S";
	who := 1;
    else
	forced := false;
	type := "G";
    end if;
    
    if type eq "S" then
	c12 := who eq 0 select Lcm(c1,c2)
	       else (who eq 1 select c1 else c2);
	dc1 := c12 div c1;
	dc2 := c12 div c2;
	s12 := SigAdd(dc1*dm1*s1,-dc2*dm2*s2 : strict := forced);
	/* s12 := dc1*dm1*s1 - dc2*dm2*s2; */
	return s12, forced select c12*m12 else m12, dc1*dm1, -dc2*dm2, "S", 0;
    else
	if forced then
	    // If one divides the other no need to make a G-pol
	    test := IsDivisibleBy(c1, c2) or IsDivisibleBy(c2, c1);
	    if test then
		return SigNull(Type(s1)),m12,0,0,"G",0;
	    end if;
	end if;

	g, dc1, dc2 := ExtendedGreatestCommonDivisor(c1,c2);
	s12 := dc1*dm1*s1 + dc2*dm2*s2;
	if SigIsNull(s12) then
	    dc1 +:= c2;
	    dc2 -:= c1;
	    assert dc1*c1 + dc2*c2 eq g;
	    s12 := dc1*dm1*s1 + dc2*dm2*s2;
	    assert not SigIsNull(s12);
	end if;
	return s12,m12,dc1*dm1,dc2*dm2, "G", g;
    end if;
end intrinsic;

/* Sig-G pairs */

intrinsic sigGPair(s1::Sig, s2::Sig)
	  -> Sig, RngMPolElt, RngMPolElt
{ Compute u1, u2 such that u1 s1 + u2 s2 = gcd(coefs(s1,s2))*lcm(mons(s1,s2))}
    i1,c1,m1 := SigExplode(s1);
    i2,c2,m2 := SigExplode(s2);
    if i1 ne i2 then
	return SigNull(Type(s1)),0,0;
    end if;
    d, u, v := Xgcd(c1,c2);
    m := Lcm(m1,m2);
    return SigNew(Type(s1),i1,d,m), u*(m div m1), v*(m div m2);
end intrinsic;

