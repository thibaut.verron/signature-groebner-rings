LC := LeadingCoefficient;
LM := LeadingMonomial;
LT := LeadingTerm;

/* Functions for reductions with signatures */

intrinsic StrongReduce(~s::Sig,~f::RngMPolElt,~G::SeqEnum[Tup],
		       ~Grem::SetEnum[RngIntElt],~changed::BoolElt
		       : mod_red := false, print_red := false)
{ Perform one regular reduction step.

Input:
s: signature of the element to reduce
f: polynomial to reduce
G: signature Gröbner basis
Grem: indices of elements not removed from the GB
changed: placeholder

Parameters:
mod_red: if true, also perform coefficient reductions
print_red: if true, print the reduction performed

Output: none

Side effects: 
If there exists g in G such that f is regular top reducible by g,
f is replaced by f - t g, and changed is set to true.
Else changed is set to false.
}
    /* changed := false; */
    ltf := LT(f);
    for i in Grem do
	ss,g := Explode(G[i]);
	if g eq 0 then
	    continue;
	end if;
	q,r := Quotrem_abs(ltf,LT(g));
	if q ne 0 and (mod_red or r eq 0)
	   and SigCmpProd(ss,q,s) lt 0 
	    then
	    changed := true;
	    f -:= q*g;
	    if print_red then
		printf "Reducing by %o * G[%o], sig=%o, new lt=%o\n",
	    	       q,i,q*ss, LT(f);
	    end if;
	    break; // or continue?
	end if;
    end for;
end intrinsic;

intrinsic StrongReduceHead(~s::Sig,~f::RngMPolElt,
			   ~G::SeqEnum[Tup], ~Grem::SetEnum[RngIntElt]
			   : mod_red := false,
			     print_red := false)
{ Regular top-reduce f as much as possible.

Input:
s: signature of the element to reduce
f: polynomial to reduce
G: signature Gröbner basis
Grem: indices of elements not removed from the GB

Parameters:
mod_red: if true, also perform coefficient reductions
print_red: if true, print each reduction performed

Output: none

Side effects: 
f is regular top-reduced modulo G.
 }
    changed := true;
    while changed and f ne 0 do
	changed := false;
	StrongReduce(~s,~f,~G,~Grem,~changed
		     : mod_red := mod_red, print_red := print_red);
    end while;
end intrinsic;

intrinsic StrongReduceTail(~s::Sig,~f::RngMPolElt,
			   ~G::SeqEnum[Tup],~Grem::SetEnum[RngIntElt]
			   : mod_red := false, print_red := false)
{ Regular tail-reduce f as much as possible.

Input:
s: signature of the element to reduce
f: polynomial to reduce
G: signature Gröbner basis
Grem: indices of elements not removed from the GB

Parameters:
mod_red: if true, also perform coefficient reductions
print_red: if true, print each reduction performed

Output: none

Side effects: 
f is regular tail-reduced modulo G.

The function assumes that f is already regular top-reduced modulo G and
does not attempt to reduce that leading term further. }
    ff := f;
    g := f-LT(f);
    f := LT(f); // We assume that f was already top-reduced
    changed := true; 
    while changed and g ne 0 do
	changed := false;
	StrongReduce(~s,~g,~G,~Grem,~changed : mod_red := mod_red,
		    print_red := print_red);
	if not changed then
	    f +:= LT(g);
	    g -:= LT(g);
	    changed := true;
	end if;
    end while;
end intrinsic;
